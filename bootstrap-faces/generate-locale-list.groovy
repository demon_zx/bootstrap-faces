import java.util.function.Consumer

//https://yadi.sk/d/O_qfNcmMZZ2qH


def file = new File("src/main/resources/META-INF/resources/bootstrap/js/moment-with-locales.min.js");

if(file.exists()){
    def reader = new BufferedReader(new InputStreamReader(new FileInputStream(file),"utf-8"));
    int i = 0;
    StringBuilder sb = new StringBuilder();
    reader.lines().forEach(new Consumer<String>() {
        @Override
        void accept(String s) {
            sb.append(s)
        }
    })
    String src = sb.toString();
    int t;
    final String prefix = '.defineLocale("';
    Map<String,String> locales = new HashMap<>();
    println "Map<String,String> locales = new HashMap<String,String>();"
    int cnt=0;
    while ((t = src.indexOf(prefix,i))!=-1){
        t+=prefix.length();
        int p = src.indexOf('"',t);
        def l = src.substring(t,p);
        Locale locale;
        if(l.contains("-")){
            String[] spl = l.split("-");
            locale = new Locale(spl[0],spl[1]);
        }else{
            locale = Locale.forLanguageTag(l);
        }
        locales.put(locale.toString(),l);
        locales.put(locale.toLanguageTag(),l);
        i = p;
    }

    locales.keySet().forEach(new Consumer<String>() {
        @Override
        void accept(String s) {
        println 'locales.put("'+s+'","'+locales.get(s)+'");'
        }
    })
}

