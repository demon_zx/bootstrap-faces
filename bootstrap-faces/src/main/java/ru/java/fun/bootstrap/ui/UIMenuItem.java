package ru.java.fun.bootstrap.ui;

import javax.faces.application.ResourceDependencies;
import javax.faces.application.ResourceDependency;
import javax.faces.component.FacesComponent;

/**
 * @author Terentjev Dmitry
 *         Date: 11.12.13
 *         Time: 10:09
 */
@FacesComponent("menuItem")
@ResourceDependencies({
        @ResourceDependency(name = "bootstrap/css/bootstrap.min.css", target = "head"),
        @ResourceDependency(name = "bootstrap/css/bootstrap-theme.min.css", target = "head"),
        @ResourceDependency(library = "bootstrap/js", name = "jquery.min.js", target = "head"),
        @ResourceDependency(library = "bootstrap/js", name = "bootstrap.min.js", target = "head")
})
public class UIMenuItem extends UIStyledOutput {

    @Override
    public String getRendererType() {
        return "ru.java.fun.bootstrap.MenuItem";
    }

    public boolean isDisabled() {
        return (Boolean) getStateHelper().eval("disabled",false);
    }

    public void setDisabled(boolean disabled) {
        getStateHelper().put("disabled",disabled);
    }

}
