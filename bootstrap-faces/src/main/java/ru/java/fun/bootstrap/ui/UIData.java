package ru.java.fun.bootstrap.ui;

import javax.faces.component.FacesComponent;
import javax.faces.component.UIOutput;

/**
 * @author Terentjev Dmitry
 *         Date: 11.12.13
 *         Time: 10:09
 */
@FacesComponent("data")
public class UIData extends UIOutput {

    @Override
    public String getRendererType() {
        return "ru.java.fun.bootstrap.Data";
    }

    public String getName() {
        return (String) getStateHelper().eval("name");
    }

    public void setName(String name) {
        getStateHelper().put("name",name);
    }
}
