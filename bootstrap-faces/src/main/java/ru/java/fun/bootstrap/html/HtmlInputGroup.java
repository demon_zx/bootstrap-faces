package ru.java.fun.bootstrap.html;

import ru.java.fun.bootstrap.ResponseWriterExtended;
import ru.java.fun.bootstrap.ui.UIInputGroup;

import javax.faces.component.UICommand;
import javax.faces.component.UIComponent;
import javax.faces.component.UIPanel;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import javax.faces.render.FacesRenderer;
import javax.faces.render.Renderer;
import java.io.IOException;

/**
 * @author Terentjev Dmitry
 *         Date: 09.01.14
 *         Time: 9:55
 */
@FacesRenderer(rendererType = "ru.java.fun.bootstrap.InputGroup", componentFamily = UIPanel.COMPONENT_FAMILY)
public class HtmlInputGroup extends Renderer {

    @Override
    public void encodeBegin(FacesContext context, UIComponent component) throws IOException {
        ResponseWriterExtended w = new ResponseWriterExtended(component, context.getResponseWriter());
        UIInputGroup c = (UIInputGroup) component;
        w.startElement("div");
        w.writeAttribute("class", "input-group " + c.getStyleClass());
        w.writeAttribute("id", c.getClientId());
        w.writeAttributeNN("style", c.getStyle());
        renderFacet(c, "left", w, context);
    }

    private void renderFacet(UIInputGroup g, String facet, ResponseWriterExtended wr, FacesContext context) throws IOException {
        UIComponent fc = g.getFacet(facet);
        if (fc != null) {
            String sc = getSc(fc);
            wr.startElement("span");
            wr.writeAttribute("class", sc);
            fc.encodeAll(context);
            wr.endElement("span");
        }
    }

    private String getSc(UIComponent fc) {
        boolean command = fc instanceof UICommand;
        if (fc instanceof UIPanel) {
            command = true;
            for (UIComponent c : fc.getChildren()) {
                command &= c instanceof UICommand;
            }
        }
        return command ? "input-group-btn" : "input-group-addon";
    }

    @Override
    public void encodeEnd(FacesContext context, UIComponent uiComponent) throws IOException {
        ResponseWriter w = context.getResponseWriter();
        renderFacet((UIInputGroup) uiComponent, "right",
                new ResponseWriterExtended(uiComponent, context.getResponseWriter()), context);
        w.endElement("div");
    }


    protected enum PropertyKeys {
        style, styleClass
    }

}
