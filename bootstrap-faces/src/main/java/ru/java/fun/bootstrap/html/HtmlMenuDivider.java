package ru.java.fun.bootstrap.html;

import ru.java.fun.bootstrap.ResponseWriterExtended;
import ru.java.fun.bootstrap.ui.UIMenuDivider;
import ru.java.fun.bootstrap.ui.UIMenuItem;

import javax.faces.component.UIComponent;
import javax.faces.component.UIOutput;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import javax.faces.render.FacesRenderer;
import javax.faces.render.Renderer;
import java.io.IOException;

/**
 * @author Terentjev Dmitry
 *         Date: 09.01.14
 *         Time: 9:55
 */
@FacesRenderer(rendererType = "ru.java.fun.bootstrap.MenuDivider", componentFamily = UIOutput.COMPONENT_FAMILY)
public class HtmlMenuDivider extends Renderer {

    @Override
    public void encodeBegin(FacesContext context, UIComponent _component) throws IOException {
        ResponseWriterExtended w = new ResponseWriterExtended(_component,context);
        UIMenuDivider c = (UIMenuDivider) _component;
        w.startElement("li");
        w.writeAttributeNN("class", "divider " + c.getStyleClass());
        w.writeAttributeNN("style", c.getStyle());
        w.endElement("li");
    }

    @Override
    public boolean getRendersChildren() {
        return false;
    }

}
