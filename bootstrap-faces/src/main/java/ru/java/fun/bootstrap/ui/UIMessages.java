package ru.java.fun.bootstrap.ui;

import javax.faces.application.ResourceDependencies;
import javax.faces.application.ResourceDependency;
import javax.faces.component.FacesComponent;

/**
 * @author Terentjev Dmitry
 *         Date: 11.12.13
 *         Time: 10:09
 */
@FacesComponent("messages")
@ResourceDependencies({
        @ResourceDependency(name = "bootstrap/css/bootstrap.min.css", target = "head"),
        @ResourceDependency(name = "bootstrap/css/bootstrap-theme.min.css", target = "head"),
        @ResourceDependency(library = "bootstrap/js", name = "jquery.min.js", target = "head")
})
public class UIMessages extends UIStyledOutput {

    @Override
    public String getRendererType() {
        return "ru.java.fun.bootstrap.Messages";
    }

    public String getFor() {
        return (String) getStateHelper().eval("for");
    }

    public void setFor(String forId) {
        getStateHelper().put("for", forId);
    }

    public boolean isGlobalOnly() {
        return (Boolean) getStateHelper().eval("globalOnly", false);
    }

    public void setGlobalOnly(boolean globalOnly) {
        getStateHelper().put("globalOnly", globalOnly);
    }

    public boolean isShowSummary() {
        return (Boolean) getStateHelper().eval("showSummary", false);
    }

    public void setShowSummary(boolean showSummary) {
        getStateHelper().put("showSummary", showSummary);
    }

    public boolean isShowDetails() {
        return (Boolean) getStateHelper().eval("showDetails", false);
    }

    public void setShowDetails(boolean showDetails) {
        getStateHelper().put("showDetails", showDetails);
    }

}
