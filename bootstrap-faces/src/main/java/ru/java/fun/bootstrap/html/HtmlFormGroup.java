package ru.java.fun.bootstrap.html;

import ru.java.fun.bootstrap.ui.*;

import javax.faces.component.UIComponent;
import javax.faces.component.UIPanel;
import javax.faces.component.html.HtmlOutputLabel;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import javax.faces.render.FacesRenderer;
import javax.faces.render.Renderer;
import java.io.IOException;

/**
 * @author Terentjev Dmitry
 *         Date: 09.01.14
 *         Time: 9:55
 */
@FacesRenderer(rendererType = "ru.java.fun.bootstrap.FormGroup", componentFamily = UIPanel.COMPONENT_FAMILY)
public class HtmlFormGroup extends Renderer {

    @Override
    public void encodeBegin(FacesContext context, UIComponent component) throws IOException {
        ResponseWriter w = context.getResponseWriter();
        w.startElement("div", component);
        UIFormGroup c = (UIFormGroup) component;
        w.writeAttribute("class", "form-group " + c.getStyleClass(), "");
        w.writeAttribute("id", c.getClientId(), "");
        if (c.getStyle() != null)
            w.writeAttribute("style", c.getStyle(), "");
    }

    @Override
    public void encodeEnd(FacesContext context, UIComponent uiComponent) throws IOException {
        ResponseWriter w = context.getResponseWriter();
        w.endElement("div");
    }

    @Override
    public boolean getRendersChildren() {
        return true;
    }

    @Override
    public void encodeChildren(FacesContext context, UIComponent uiComponent) throws IOException {
        UIFormGroup cmp = (UIFormGroup) uiComponent;
        String[] columns = cmp.getColumns().split(",");
        String oo = cmp.getOffsets();
        String[] offsets = oo == null ? new String[0] : oo.split(",");
        ResponseWriter w = context.getResponseWriter();
        int i = 0;
        for (UIComponent c : uiComponent.getChildren()) {
            boolean ignore = c instanceof UIAutocomplete;
            if (!ignore) {
                boolean isLabel = c instanceof HtmlOutputLabel;
                boolean notWrap = isLabel
                        || c instanceof HtmlOutputText
                        || c instanceof UIFormGroupColumn;
                boolean notControl = c instanceof UIButtonGroup
                        || c instanceof UICrop
                        || c instanceof UIInputGroup;
//            boolean isWrapper = c instanceof UIFormGroupColumn;
                String column = null;
                if (i < columns.length)
                    column = columns[i];
                String offset = null;
                if (i < offsets.length) {
                    offset = offsets[i];
                    if (offset.isEmpty() || "0".equalsIgnoreCase(offset))
                        offset = null;
                }
                String styleClass = notWrap ? (String) c.getAttributes().get("styleClass") : null;
                if (column != null)
                    styleClass = appendStyleClass(styleClass, "col-lg-" + column);
                if (offset != null)
                    styleClass = appendStyleClass(styleClass, "col-lg-offset-" + offset);
                if (isLabel)
                    styleClass = appendStyleClass(styleClass, "control-label");
                if (styleClass != null && notWrap)
                    c.getAttributes().put("styleClass", styleClass);
                if (notWrap) {
                    c.encodeAll(context);
                } else {
                    w.startElement("div", null);
                    w.writeAttribute("class", styleClass, "");
                    if (!notControl) {
                        String sc = (String) c.getAttributes().get("styleClass");
                        sc = sc == null ? "form-control" : (sc.contains("form-control") ? sc : sc + " form-control");
                        c.getAttributes().put("styleClass", sc);
                    }
                    c.encodeAll(context);
                    w.endElement("div");
                }
                i++;
                if (i >= columns.length) {
                    i = 0;
                }
            } else {
                c.encodeAll(context);
            }
        }
    }

    private String appendStyleClass(String styleClass, String value) {
        return styleClass != null && !styleClass.contains(value) ? styleClass + " " + value : value;
    }

}
