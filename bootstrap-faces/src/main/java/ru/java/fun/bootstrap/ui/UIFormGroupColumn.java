package ru.java.fun.bootstrap.ui;

import javax.faces.component.FacesComponent;
import javax.faces.component.UIPanel;

/**
 * @author Terentjev Dmitry
 *         Date: 11.12.13
 *         Time: 10:09
 */
@FacesComponent("formGroupColumn")
public class UIFormGroupColumn extends UIPanel {

    @Override
    public String getRendererType() {
        return "ru.java.fun.bootstrap.FormGroupColumn";
    }

    public String getStyle() {
        return (String) getStateHelper().eval("style");
    }

    public void setStyle(String style) {
        getStateHelper().put("style",style);
    }

    public String getStyleClass() {
        return (String) getStateHelper().eval("styleClass","");
    }

    public void setStyleClass(String styleClass) {
        getStateHelper().put("styleClass",styleClass);
    }


}
