package ru.java.fun.bootstrap;

import ru.java.fun.bootstrap.util.FacesUtils;

/**
 * @author Terentjev Dmitry
 *         Date: 12.08.2014
 *         Time: 12:22
 */
public class Messages {
    public static final String BUNDLE = "bootstrap";
    public static final String BASE = "ru.java.fun";
    public static final String ERROR = "error";
    public static final String CONFIRM_TITLE = "confirm.title";
    public static final String CONFIRM_MESSAGE = "confirm.message";
    public static final String CONFIRM_BUTTON_YES = "confirm.button.yes";
    public static final String CONFIRM_BUTTON_NO = "confirm.button.no";

    public static String getMessage(String id){
        return FacesUtils.getBundleValue(BUNDLE,BASE+"."+id,"NOT_FOUND_RESOURCE");
    }

}
