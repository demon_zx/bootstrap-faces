package ru.java.fun.bootstrap.html;

import ru.java.fun.bootstrap.ResponseWriterExtended;
import ru.java.fun.bootstrap.ui.UISeverity;

import javax.faces.component.UIComponent;
import javax.faces.component.UIOutput;
import javax.faces.context.FacesContext;
import javax.faces.render.FacesRenderer;
import javax.faces.render.Renderer;
import java.io.IOException;

/**
 * @author Terentjev Dmitry
 *         Date: 09.01.14
 *         Time: 9:55
 */
@FacesRenderer(rendererType = "ru.java.fun.bootstrap.Severity", componentFamily = UIOutput.COMPONENT_FAMILY)
public class HtmlSeverity extends Renderer {

    @Override
    public void encodeBegin(FacesContext context, UIComponent _component) throws IOException {
        ResponseWriterExtended w = new ResponseWriterExtended(_component, context.getResponseWriter());
        UISeverity c = (UISeverity) _component;
        w.startElement("span");
        w.writeAttribute("style", "display:none");
        w.writeAttribute("id", c.getClientId());
        w.writeText(context.getMaximumSeverity());
        w.endElement("span");

    }

    @Override
    public boolean getRendersChildren() {
        return false;
    }

}
