package ru.java.fun.bootstrap.html;

import ru.java.fun.bootstrap.ResponseWriterExtended;
import ru.java.fun.bootstrap.ui.UIModal;
import ru.java.fun.bootstrap.ui.UISeverity;
import ru.java.fun.bootstrap.util.RenderKitUtils;

import javax.faces.component.UIComponent;
import javax.faces.component.UIPanel;
import javax.faces.component.html.HtmlForm;
import javax.faces.context.FacesContext;
import javax.faces.render.FacesRenderer;
import javax.faces.render.Renderer;
import java.io.IOException;

/**
 * @author Terentjev Dmitry
 *         Date: 09.01.14
 *         Time: 9:55
 */
@FacesRenderer(rendererType = "ru.java.fun.bootstrap.Modal", componentFamily = UIPanel.COMPONENT_FAMILY)
public class HtmlModal extends Renderer {

    @Override
    public void encodeBegin(FacesContext context, UIComponent _component) throws IOException {
        ResponseWriterExtended w = new ResponseWriterExtended(_component, context.getResponseWriter());
        UIModal c = (UIModal) _component;
        w.startElement("div");
        w.writeAttribute("id", c.getClientId());
        w.writeAttribute("modal-id", c.getId());
        w.writeAttribute("class", "modal " + c.getStyleClass());
        w.writeAttribute("style", c.getStyle());
        w.writeAttribute("tabindex", "-1");
        w.writeAttribute("data-backdrop", c.isCloseable() && c.isHideOnMask() ? "true" : "static");
        w.writeAttribute("data-id-severity", getSeverityId(c));
        String labelId = c.getClientId() + ":label";
        w.writeAttribute("aria-labeledby", labelId);
        w.writeAttribute("role", "dialog");
        w.writeAttribute("aria-hidden", c.isCloseable() && c.isHideOnMask());

        w.startElement("div");
        w.writeAttribute("class", "modal-dialog");

        if (!"600px".equalsIgnoreCase(c.getWidth())) {
            w.writeAttribute("style", "width:" + c.getWidth());
        }

        w.startElement("div");
        w.writeAttribute("id", c.getClientId() + ":modal");
        w.writeAttribute("class", "modal-content");

        w.startElement("div");
        w.writeAttribute("id", c.getClientId() + ":header");
        w.writeAttributeNN("style", c.getHeaderStyle());
        w.writeAttribute("class", "modal-header " + c.getHeaderStyleClass());
        UIComponent header = c.getFacet("header");
        if (header == null) {
            if (c.isCloseable()) {
                w.startElement("button");
                w.writeAttribute("type", "button");
                w.writeAttribute("class", "close");
                w.writeAttribute("onclick", "jsf.bootstrap.modal(this).hide()");
                context.getResponseWriter().write("&#215;");
                w.endElement("button");
            }
            w.startElement("h4");
            w.writeAttribute("class", "modal-title");
            w.writeAttribute("id", labelId);
            w.writeText(c.getTitle());
            w.endElement("h4");
        } else {
            header.encodeAll(context);
        }
        w.endElement("div");

        w.startElement("div");
        w.writeAttribute("id", c.getClientId() + ":body");
        StringBuilder hs = new StringBuilder();
        if (c.getHeight() != null && !c.getHeight().trim().isEmpty()) {
            hs.append("height:").append(c.getHeight()).append(";overflow-y:auto;");
        }
        if (c.getBodyStyle() != null && !c.getBodyStyle().trim().isEmpty()) {
            hs.append(c.getBodyStyle());
        }
        String hss = hs.toString().trim();
        if (!hss.isEmpty()) {
            w.writeAttribute("style", hss);
        }
        w.writeAttribute("class", "modal-body " + c.getBodyStyleClass());
    }

    @Override
    public boolean getRendersChildren() {
        return true;
    }


    @Override
    public void encodeEnd(FacesContext context, UIComponent uiComponent) throws IOException {
        ResponseWriterExtended w = new ResponseWriterExtended(uiComponent, context.getResponseWriter());
        UIModal c = (UIModal) uiComponent;
        w.endElement("div");
        UIComponent footer = c.getFacet("footer");
        if (footer != null) {
            w.startElement("div");
            w.writeAttribute("id", c.getClientId() + ":footer");
            w.writeAttribute("class", "modal-footer");
            footer.encodeAll(context);
            w.endElement("div");
        }
        w.endElement("div");
        w.endElement("div");
        w.endElement("div");
        w.startElement("script");
        w.writeAttribute("type", "text/javascript");
        String id = c.getClientId();
        String show = RenderKitUtils.scriptHandler(context, c, null, "show", c.getOnshow(), "");
        String hide = RenderKitUtils.scriptHandler(context, c, null, "hide", c.getOnhide(), "");
        String add = "";
        if (!show.isEmpty()) {
            add += ".onShow(function(){" + show + "})";
        }
        if (!hide.isEmpty()) {
            add += ".onHide(function(){" + hide + "})";
        }
        w.writeText("$(function(){\njsf.bootstrap.modal('" + id + "')" + add + "}\n)");
        w.endElement("script");
    }

    private String getSeverityId(UIModal c) {
        String errId = findSeverity(c);
        if (errId != null) {
            return errId.replaceAll(":", "\\\\:");
        }
        return "";
    }

    private String findSeverity(UIComponent c) {
        if (c instanceof UISeverity) {
            return c.getClientId();
        }
        for (UIComponent component : c.getChildren()) {
            String cid = findSeverity(component);
            if (cid != null) {
                return cid;
            }
        }
        return null;
    }

    private UIComponent findForm(UIComponent cmp) {
        if (cmp instanceof HtmlForm)
            return cmp;
        if (cmp.getParent() != null)
            return findForm(cmp.getParent());
        return null;
    }
}
