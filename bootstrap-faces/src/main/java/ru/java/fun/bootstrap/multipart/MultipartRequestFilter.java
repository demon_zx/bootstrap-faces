package ru.java.fun.bootstrap.multipart;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import ru.java.fun.bootstrap.Bootstrap;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Terentjev Dmitry
 *         Date: 07.04.2014
 *         Time: 14:05
 */
//@WebFilter("/*")
public class MultipartRequestFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest r = (HttpServletRequest) servletRequest;
        if (ServletFileUpload.isMultipartContent(r)) {
            File tmp = (File) r.getServletContext().getAttribute("javax.servlet.context.tempdir");
            DiskFileItemFactory factory = new DiskFileItemFactory();
            int ms = Integer.parseInt(r.getServletContext().getAttribute(Bootstrap.FILE_UPLOAD_SIZE_THRESHOLD).toString());
            factory.setSizeThreshold(ms);
            factory.setRepository(tmp);
            ServletFileUpload fu = new ServletFileUpload(factory);
            MultipartRequest mr = new MultipartRequest(r);
            try {
                Map<String, List<FileItem>> files = fu.parseParameterMap(r);
                Map<String, Integer> cnt = new HashMap<String, Integer>();
                for (String name : files.keySet()) {
                    for (FileItem fi : files.get(name)) {
                        if (fi.isFormField()) {
                            mr.addParameter(fi.getFieldName(), fi.getString());
                        } else {

                            if (fi.getFieldName().endsWith("[]")) {
                                String id = fi.getFieldName().substring(0, fi.getFieldName().length() - 2);
                                Integer c = cnt.get(id);
                                if (c == null) {
                                    c = 0;
                                }
                                mr.addParameter(id + ":" + c++, ((DiskFileItem) fi).getStoreLocation().getAbsolutePath());
                                cnt.put(id, c);
                            } else {
                                DiskFileItem di = (DiskFileItem) fi;
                                if (di.isInMemory()) {
                                    OutputStream os = null;
                                    try {
                                        os = new FileOutputStream(di.getStoreLocation());
                                        os.write(di.get());
                                        os.flush();
                                    } finally {
                                        if (os != null)
                                            os.close();
                                    }
                                }
                                mr.addParameter(fi.getFieldName(), di.getStoreLocation().getAbsolutePath());
                                mr.setAttribute(fi.getFieldName() + ":file:0", fi);
                            }
                        }
                    }
                }
                for (String id : cnt.keySet()) {
                    mr.addParameter(id, String.valueOf(cnt.get(id)));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            filterChain.doFilter(mr, servletResponse);
//            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}
