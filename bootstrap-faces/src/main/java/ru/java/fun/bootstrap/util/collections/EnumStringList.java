package ru.java.fun.bootstrap.util.collections;

import java.util.ArrayList;

/**
 * @author Terentjev Dmitry
 *         Date: 06.05.2014
 *         Time: 11:34
 */
public class EnumStringList extends ArrayList<String> {

    public EnumStringList(Enum[] enumValues) {
        for (Enum t : enumValues) {
            add(t.name());
        }
    }
}
