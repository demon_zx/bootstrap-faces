package ru.java.fun.bootstrap.function;

import ru.java.fun.bootstrap.util.FacesUtils;

import javax.faces.component.UIComponent;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author Terentjev Dmitry
 *         Date: 25.04.2014
 *         Time: 10:02
 */
public final class Functions {

    public static boolean containsId(String id) {
        return containsIdComponents(id, FacesUtils.getFaces().getViewRoot().getChildren());
    }

    private static boolean containsIdComponents(String clientId, List<UIComponent> list) {
        for (UIComponent c : list) {
            if (c.getClientId().equals(clientId)) {
                return true;
            }
            boolean r = containsIdComponents(clientId, c.getChildren());
            if (r)
                return true;
        }
        return false;
    }

    public static String dateFormat(String pattern, Date date) {
        return new SimpleDateFormat(pattern).format(date);
    }
}
