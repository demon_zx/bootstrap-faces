package ru.java.fun.bootstrap.ui;

import javax.faces.application.ResourceDependencies;
import javax.faces.application.ResourceDependency;
import javax.faces.component.FacesComponent;
import javax.faces.component.UIPanel;

/**
 * @author Terentjev Dmitry
 *         Date: 11.12.13
 *         Time: 10:09
 */
@FacesComponent("accordion")
@ResourceDependencies({
        @ResourceDependency(name = "bootstrap/css/bootstrap.min.css", target = "head"),
        @ResourceDependency(name = "bootstrap/css/bootstrap-theme.min.css", target = "head"),
        @ResourceDependency(library = "bootstrap/js", name = "jquery.min.js", target = "head"),
        @ResourceDependency(library = "bootstrap/js", name = "bootstrap.min.js", target = "head")
})
public class UIAccordion extends UIPanel {

    @Override
    public String getRendererType() {
        return "ru.java.fun.bootstrap.Accordion";
    }

    public String getStyle() {
        return (String) getStateHelper().eval("style");
    }

    public void setStyle(String style) {
        getStateHelper().put("style",style);
    }

    public String getStyleClass() {
        return (String) getStateHelper().eval("styleClass","");
    }

    public void setStyleClass(String styleClass) {
        getStateHelper().put("styleClass",styleClass);
    }
}
