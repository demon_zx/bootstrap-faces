package ru.java.fun.bootstrap.ui;

import javax.faces.application.ResourceDependencies;
import javax.faces.application.ResourceDependency;
import javax.faces.component.FacesComponent;
import javax.faces.component.UIInput;
import javax.faces.component.behavior.ClientBehavior;
import javax.faces.component.behavior.ClientBehaviorHolder;
import javax.faces.context.FacesContext;
import javax.faces.convert.ConverterException;
import java.util.*;

/**
 * @author Terentjev Dmitry
 *         Date: 11.12.13
 *         Time: 10:09
 */
@FacesComponent("select")
@ResourceDependencies({
        @ResourceDependency(name = "bootstrap/css/bootstrap.min.css", target = "head"),
        @ResourceDependency(name = "bootstrap/css/bootstrap-theme.min.css", target = "head"),
        @ResourceDependency(name = "bootstrap/css/bootstrap-select.min.css", target = "head"),
        @ResourceDependency(library = "bootstrap/js", name = "jquery.min.js", target = "head"),
        @ResourceDependency(library = "javax.faces", name = "jsf.js"),
        @ResourceDependency(library = "bootstrap/js", name = "bootstrap.min.js", target = "head"),
        @ResourceDependency(library = "bootstrap/js", name = "jsf-bs.js", target = "head"),
        @ResourceDependency(library = "bootstrap/js", name = "bootstrap-select.min.js", target = "head")
})
public class UISelect extends UIInput implements ClientBehaviorHolder {

    private static final Collection<String> EVENT_NAMES = Collections.unmodifiableCollection(Arrays.asList("select"));

    @Override
    public String getRendererType() {
        return "ru.java.fun.bootstrap.Select";
    }

    public String getTitle() {
        return (String) getStateHelper().eval("title");
    }

    public void setTitle(String title) {
        getStateHelper().put("title", title);
    }

    public String getStyle() {
        return (String) getStateHelper().eval("style");
    }

    public void setStyle(String style) {
        getStateHelper().put("style", style);
    }

    public String getWidth() {
        return (String) getStateHelper().eval("width");
    }

    public void setWidth(String width) {
        getStateHelper().put("width", width);
    }

    public String getStyleClass() {
        return (String) getStateHelper().eval("styleClass", "");
    }

    public void setStyleClass(String styleClass) {
        getStateHelper().put("styleClass", styleClass);
    }

    public boolean isDisabled() {
        return (Boolean) getStateHelper().eval("disabled", false);
    }

    public void setDisabled(boolean disabled) {
        getStateHelper().put("disabled", disabled);
    }

    public boolean isSearch() {
        return (Boolean) getStateHelper().eval("search", false);
    }

    public void setSearch(boolean search) {
        getStateHelper().put("search", search);
    }

    @Override
    public Collection<String> getEventNames() {
        return EVENT_NAMES;
    }

    @Override
    public String getDefaultEventName() {
        return "select";
    }

    @Override
    public Object getConvertedValue(FacesContext context, Object submittedValue) throws ConverterException {
        String newValue = (String) submittedValue;
        if (null != getConverter()) {
            return getConverter().getAsObject(context, this, newValue);
        }
        return newValue;
    }

    @Override
    public void decode(FacesContext context) {
        Map<String, String> pars = context.getExternalContext().getRequestParameterMap();
        String val = pars.get(getClientId());
        setValue(getConvertedValue(context, val));
        Map<String, List<ClientBehavior>> mapBeh = getClientBehaviors();
        if (mapBeh.isEmpty())
            return;

        String event = pars.get("javax.faces.behavior.event");
        List<ClientBehavior> beh = mapBeh.get(event);
        if (beh != null && !beh.isEmpty()) {
            String source = pars.get("javax.faces.source");
            String clientId = getClientId(context);
            if (clientId.equals(source)) {
                for (ClientBehavior cb : beh) {
                    cb.decode(context, this);
                }
            }
        }
    }
}
