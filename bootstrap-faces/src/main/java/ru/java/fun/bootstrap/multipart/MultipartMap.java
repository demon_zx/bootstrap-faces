package ru.java.fun.bootstrap.multipart;

import java.util.*;

/**
 * @author Terentjev Dmitry
 *         Date: 07.04.2014
 *         Time: 16:41
 */
public class MultipartMap implements Map<String, String[]> {

    private Map<String, List<String>> container = new HashMap<String, List<String>>();

    public void add(String key, String value) {
        List<String> list = container.get(key);
        if (list == null) {
            list = new ArrayList<String>();
            container.put(key, list);
        }
        list.add(value);
    }

    @Override
    public int size() {
        return container.size();
    }

    @Override
    public boolean isEmpty() {
        return container.isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
        return container.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        return container.containsValue(value);
    }

    @Override
    public String[] get(Object key) {
        List<String> tmp = container.get(key);
        return tmp == null ? null : tmp.toArray(new String[tmp.size()]);
    }

    @Override
    public String[] put(String key, String[] value) {
        container.put(key, new ArrayList<String>(Arrays.asList(value)));
        return get(key);
    }

    @Override
    public String[] remove(Object key) {
        String[] tmp = get(key);
        container.remove(key);
        return tmp;
    }

    @Override
    public void putAll(Map<? extends String, ? extends String[]> m) {
        for (String key : m.keySet()) {
            put(key, m.get(key));
        }
    }

    @Override
    public void clear() {
        container.clear();
    }

    @Override
    public Set<String> keySet() {
        return container.keySet();
    }

    @Override
    public Collection<String[]> values() {
        List<String[]> t = new ArrayList<String[]>();
        for (String key : keySet()) {
            t.add(get(key));
        }
        return t;
    }

    @Override
    public Set<Entry<String, String[]>> entrySet() {
        Set<Entry<String, String[]>> t = new HashSet<Entry<String, String[]>>();
        for (String key : keySet()) {
            t.add(new AbstractMap.SimpleEntry<String, String[]>(key, get(key)));
        }
        return t;
    }
}
