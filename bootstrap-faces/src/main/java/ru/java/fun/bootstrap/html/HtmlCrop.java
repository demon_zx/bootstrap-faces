package ru.java.fun.bootstrap.html;

import ru.java.fun.bootstrap.ResponseWriterExtended;
import ru.java.fun.bootstrap.ui.UICrop;

import javax.faces.component.UIComponent;
import javax.faces.component.UIOutput;
import javax.faces.context.FacesContext;
import javax.faces.render.FacesRenderer;
import javax.faces.render.Renderer;
import java.io.IOException;

/**
 * @author Terentjev Dmitry
 *         Date: 09.01.14
 *         Time: 9:55
 */
@FacesRenderer(rendererType = "ru.java.fun.bootstrap.Crop", componentFamily = UIOutput.COMPONENT_FAMILY)
public class HtmlCrop extends Renderer {

    @Override
    public void encodeBegin(FacesContext context, UIComponent _component) throws IOException {
        ResponseWriterExtended w = new ResponseWriterExtended(_component, context.getResponseWriter());
        UICrop c = (UICrop) _component;
        w.startElement("div");
        w.writeAttribute("class", "-jbs-crop " + c.getStyleClass()+(c.isSingleLine()?"-jbs-crop-wrap":""));
        w.writeAttribute("style", "height: " + c.getHeight() + "px; " + c.getStyle());
        w.writeAttribute("id", c.getClientId());
        w.writeAttribute("data-single",c.isSingleLine());
        w.startElement("div");
    }

    @Override
    public boolean getRendersChildren() {
        return true;
    }

    @Override
    public void encodeEnd(FacesContext context, UIComponent uiComponent) throws IOException {
        ResponseWriterExtended w = new ResponseWriterExtended(uiComponent, context);
        w.endElement("div");
        w.endElement("div");
        w.startElement("script");
        w.writeAttribute("type", "text/javascript");
        String id = uiComponent.getClientId().replaceAll(":", "\\\\\\\\:");
        w.writeText("$(function(){jsf.bootstrap.crop('#" + id + "');})");
        w.endElement("script");
    }
}
