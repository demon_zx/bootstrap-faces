package ru.java.fun.bootstrap.html;

import ru.java.fun.bootstrap.ui.UIMenu;
import ru.java.fun.bootstrap.ui.UIMenuGroup;

import javax.faces.component.UIComponent;
import javax.faces.component.UIPanel;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import javax.faces.render.FacesRenderer;
import javax.faces.render.Renderer;
import java.io.IOException;

/**
 * @author Terentjev Dmitry
 *         Date: 09.01.14
 *         Time: 9:55
 */
@FacesRenderer(rendererType = "ru.java.fun.bootstrap.MenuGroup", componentFamily = UIPanel.COMPONENT_FAMILY)
public class HtmlMenuGroup extends Renderer {

    @Override
    public void encodeBegin(FacesContext context, UIComponent _component) throws IOException {
        ResponseWriter w = context.getResponseWriter();
        UIMenuGroup c = (UIMenuGroup) _component;
        boolean inmenu = c.getParent() instanceof UIMenu;
        w.startElement(inmenu ? "li" : "div", c);
        if (inmenu) {
            w.writeAttribute("class", "dropdown", "");
        } else {
            w.writeAttribute("class", "btn-group", "");
        }

        UIComponent facet = c.getFacet("button");

        if (facet != null) {
            facet.encodeAll(context);
        } else {
            String addon = inmenu ? "" : "btn btn-" + c.getContext() + " ";
            w.startElement("a", c);
            w.writeAttribute("id", c.getClientId(), "");
            w.writeAttribute("href", "#", "");
            w.writeAttribute("class", addon + "dropdown-toggle", "");
            w.writeAttribute("role", "button", "");
            w.writeAttribute("data-toggle", "dropdown", "");
            if (c.getValue() != null)
                w.writeText(c.getValue(), "");

            w.startElement("b", c);
            w.writeAttribute("class", "caret", "");
            w.endElement("b");
            w.endElement("a");
        }

        w.startElement("ul", c);
        w.writeAttribute("class", "dropdown-menu " + c.getStyleClass(), "");
        w.writeAttribute("style", c.getStyle(), "");
        w.writeAttribute("role", "menu", "");
    }

    @Override
    public boolean getRendersChildren() {
        return true;
    }

    @Override
    public void encodeEnd(FacesContext context, UIComponent uiComponent) throws IOException {
        ResponseWriter w = context.getResponseWriter();
        w.endElement("ul");
        boolean inmenu = uiComponent.getParent() instanceof UIMenu;
        w.endElement(inmenu ? "li" : "div");
        UIMenuGroup c = (UIMenuGroup) uiComponent;

        w.startElement("script", c);
        w.writeAttribute("type", "text/javascript", "");
        String id = c.getClientId().replaceAll(":", "\\\\\\\\:");
        w.writeText("$(function(){$('#" + id + "').dropdown()})", "");
        w.endElement("script");
    }
}
