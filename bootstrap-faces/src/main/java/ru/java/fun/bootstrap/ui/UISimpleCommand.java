package ru.java.fun.bootstrap.ui;

import ru.java.fun.bootstrap.util.RenderKitUtils;

import javax.faces.component.UICommand;
import javax.faces.component.behavior.ClientBehavior;
import javax.faces.component.behavior.ClientBehaviorHolder;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import java.util.*;

/**
 * @author Terentjev Dmitry
 *         Date: 07.02.14
 *         Time: 14:17
 */
public abstract class UISimpleCommand extends UICommand implements ClientBehaviorHolder {

    protected static final Collection<String> EVENT_NAMES = Collections.unmodifiableCollection(Arrays.asList("action", "submit"));

    @Override
    public Collection<String> getEventNames() {
        return EVENT_NAMES;
    }

    @Override
    public String getDefaultEventName() {
        return "action";
    }


    @Override
    public void decode(FacesContext context) {
        if (isDisabled()) {
            return;
        }
        decodeBehaviors(context);
        if (RenderKitUtils.isClicked(context, getClientId())) {
            queueEvent(new ActionEvent(this));
        }
    }

    protected void decodeBehaviors(FacesContext context) {
        Map<String, String> pars = context.getExternalContext().getRequestParameterMap();
        Map<String, List<ClientBehavior>> mapBeh = getClientBehaviors();
        String event = pars.get("javax.faces.behavior.event");
        List<ClientBehavior> beh = mapBeh.get(event);
        if (beh != null && !beh.isEmpty()) {
            String source = pars.get("javax.faces.source");
            String clientId = getClientId(context);
            if (clientId.equals(source)) {
                for (ClientBehavior cb : beh) {
                    cb.decode(context, this);
                }
            }
        }
    }

    public abstract boolean isDisabled();

}
