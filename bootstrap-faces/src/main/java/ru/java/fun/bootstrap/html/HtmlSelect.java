package ru.java.fun.bootstrap.html;

import ru.java.fun.bootstrap.ResponseWriterExtended;
import ru.java.fun.bootstrap.ui.UISelect;
import ru.java.fun.bootstrap.util.RenderKitUtils;

import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.ValueHolder;
import javax.faces.component.behavior.AjaxBehavior;
import javax.faces.component.behavior.ClientBehavior;
import javax.faces.component.behavior.ClientBehaviorContext;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.model.SelectItem;
import javax.faces.render.FacesRenderer;
import java.io.IOException;
import java.util.*;

/**
 * @author Terentjev Dmitry
 *         Date: 09.01.14
 *         Time: 9:55
 */
@FacesRenderer(rendererType = "ru.java.fun.bootstrap.Select", componentFamily = UIInput.COMPONENT_FAMILY)
public class HtmlSelect extends RendererExtended<UISelect> {

    //todo если сделать render="@all" то перестает работать селект
    @Override
    public void encodeBegin(FacesContext context, UISelect c, ResponseWriterExtended w) throws IOException {
        w.start("select")
                .id()
                .attr("name", c.getClientId())
                .attrNN("title", c.getTitle())
                .styleClass("selectpicker", c.getStyleClass())
                .style(c.getStyle())
        .attrNN("data-width",c.getWidth());
        if (c.isDisabled()) {
            w.attr("disabled", "disabled");
        }
        if (c.isSearch()) {
            w.attr("data-live-search", c.isSearch());
        }

        Map<String, List<ClientBehavior>> map = c.getClientBehaviors();
        if (map.containsKey("select")) {
            ClientBehavior cb = map.get("select").get(0);
            if (cb instanceof AjaxBehavior) {
                AjaxBehavior ab = (AjaxBehavior) cb;
                ClientBehaviorContext.Parameter p = new ClientBehaviorContext.Parameter("execute", unwrap(ab.getExecute()));
                ClientBehaviorContext.Parameter r = new ClientBehaviorContext.Parameter("render", unwrap(ab.getRender()));
                ClientBehaviorContext ctx = ClientBehaviorContext.createClientBehaviorContext(context,
                        c, "select", c.getClientId(), Arrays.asList(p, r));
                String click = ab.getScript(ctx) + ";return false;";
                w.attr("onchange", click);
            }
        }
        boolean sel = false;
        for (Iterator<SelectItem> it = RenderKitUtils.getSelectItems(context, c); it.hasNext(); ) {
            SelectItem si = it.next();
            w.start("option");
            String value = getValueAsString(context, c, si.getValue());
            w.attr("value", value);
            if (si.isDisabled())
                w.attr("disabled", "disabled");
            w.attrNN("title", si.getDescription());
            String lb = si.getLabel();
            if (lb == null) {
                lb = "" + si.getValue();
            }
            if (!sel && c.getValue() != null && c.getValue().toString().equals(si.getValue().toString())) {
                w.attr("selected", "selected");
                sel = true;
            }
            if (si.isEscape()) {
                w.text(lb);
            } else {
                w.attr("data-content", lb).text(value);
            }
            w.end("option");
        }

    }


    protected String getValueAsString(FacesContext context, UIComponent component, Object submittedValue) throws ConverterException {
        if (submittedValue == null) {
            return "";
        } else {
            if (component instanceof ValueHolder) {
                Converter converter = ((ValueHolder) component).getConverter();
                if (converter != null) {
                    return converter.getAsString(context, component, submittedValue);
                }
            }
            return submittedValue.toString();
        }
    }

    private String unwrap(Collection<String> collection) {
        StringBuilder sb = new StringBuilder();
        for (String s : collection) {
            sb.append(s).append(" ");
        }
        return sb.toString().trim();
    }

    @Override
    public void encodeEnd(FacesContext context, UISelect c, ResponseWriterExtended w) throws IOException {
        w.endElement("select");
        w.startElement("script");
        w.writeAttribute("type", "text/javascript");
        String id = c.getClientId().replaceAll(":", "\\\\\\\\:");
        w.writeText("$(function(){\n" +
                "$('#" + id + "').selectpicker();\n" +
                "\n})");
        w.endElement("script");
    }
}
