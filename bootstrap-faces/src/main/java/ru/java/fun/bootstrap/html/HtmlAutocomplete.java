package ru.java.fun.bootstrap.html;

import ru.java.fun.bootstrap.ResponseWriterExtended;
import ru.java.fun.bootstrap.ui.UIAutocomplete;

import javax.faces.component.UIComponent;
import javax.faces.component.UIPanel;
import javax.faces.context.FacesContext;
import javax.faces.render.FacesRenderer;
import javax.faces.render.Renderer;
import java.io.IOException;

/**
 * @author Terentjev Dmitry
 *         Date: 09.01.14
 *         Time: 9:55
 */
@FacesRenderer(rendererType = "ru.java.fun.bootstrap.Autocomplete", componentFamily = UIPanel.COMPONENT_FAMILY)
public class HtmlAutocomplete extends Renderer {

    @Override
    public void encodeBegin(FacesContext context, UIComponent _component) throws IOException {
        ResponseWriterExtended w = new ResponseWriterExtended(_component, context.getResponseWriter());
        UIAutocomplete c = (UIAutocomplete) _component;
        w.startElement("script" );
        w.writeAttribute("id", c.getClientId());
        w.writeAttribute("type", "text/javascript" );
        String id = "[id $=" + c.getFor().replaceAll(":", "\\\\\\\\:" ) + "]";
        w.writeText(String.format("$(function(){jsf.bootstrap.autocomplete('%s',%s,%s,%s)})", id, c.getTags(), c.getLimit(), c.getMinLength()));
        w.endElement("script" );
    }
}
