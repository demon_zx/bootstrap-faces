package ru.java.fun.bootstrap.datatable.sorting;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Terentjev Dmitry
 *         Date: 03.02.14
 *         Time: 15:05
 */
public class SorterBuilder<T> {

    private String defaultField;
    private boolean defaultAscend;
    private List<SortableComparator<T>> comparatorList = new ArrayList<SortableComparator<T>>();
    private Map<String,String> mapping = new HashMap<String, String>();

    public SorterBuilder<T> addComparator(SortableComparator<T> comparator) {
        comparatorList.add(comparator);
        return this;
    }

    public SorterBuilder<T> addDefaultComparator(SortableComparator<T> comparator, boolean ascend) {
        defaultField = comparator.getFieldName();
        defaultAscend = ascend;
        return addComparator(comparator);
    }

    public SorterBuilder<T> orderCssMap(SortableState state, String mapTo) {
        mapping.put(state.toString(),mapTo);
        return this;
    }

    public Sorter<T> build() {
        return new Sorter<T>(comparatorList,mapping,defaultField,defaultAscend);
    }

    public SorterBuilder<T> orderCssMapGlyphiconsAttributes() {
        return this.orderCssMap(SortableState.unsorted, "sort")
                .orderCssMap(SortableState.ascending, "sort-by-attributes")
                .orderCssMap(SortableState.descending, "sort-by-attributes-alt");
    }

    public SorterBuilder<T> orderCssMapGlyphiconsAlphabets() {
        return this.orderCssMap(SortableState.unsorted, "sort")
                .orderCssMap(SortableState.ascending, "sort-by-alphabet")
                .orderCssMap(SortableState.descending, "sort-by-alphabet-alt");
    }

    public SorterBuilder<T> orderCssMapGlyphiconsOrders() {
        return this.orderCssMap(SortableState.unsorted, "sort")
                .orderCssMap(SortableState.ascending, "sort-by-order")
                .orderCssMap(SortableState.descending, "sort-by-order-alt");
    }

    public SorterBuilder<T> orderCssMapGlyphiconsChevrons() {
        return this.orderCssMap(SortableState.unsorted, "remove")
                .orderCssMap(SortableState.ascending, "chevron-down")
                .orderCssMap(SortableState.descending, "chevron-up");
    }
}
