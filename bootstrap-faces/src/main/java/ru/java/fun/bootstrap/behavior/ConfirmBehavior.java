package ru.java.fun.bootstrap.behavior;

import ru.java.fun.bootstrap.Messages;

import javax.el.ELContext;
import javax.el.ELException;
import javax.el.ValueExpression;
import javax.faces.FacesException;
import javax.faces.component.UIComponentBase;
import javax.faces.component.behavior.ClientBehaviorBase;
import javax.faces.component.behavior.ClientBehaviorContext;
import javax.faces.component.behavior.FacesBehavior;
import javax.faces.context.FacesContext;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Terentjev Dmitry
 *         Date: 03.04.2014
 *         Time: 11:35
 */
@FacesBehavior("ru.java.fun.bootstrap.ConfirmBehavior")
public class ConfirmBehavior extends ClientBehaviorBase {
    public static final String MESSAGE = "message";
    public static final String STYLE = "style";
    public static final String STYLE_CLASS = "styleClass";
    public static final String BUTTON_YES = "buttonYes";
    public static final String BUTTON_NO = "buttonNo";
    public static final String TITLE = "title";
    private String message, style, styleClass, buttonYes, buttonNo, title;
    private Map<String, ValueExpression> bindings;

    // Utility for saving bindings state
    private static Object saveBindings(FacesContext context,
                                       Map<String, ValueExpression> bindings) {

        // Note: This code is copied from UIComponentBase.  In a future
        // version of the JSF spec, it would be useful to define a
        // attribute/property/bindings/state helper object that can be
        // shared across components/behaviors/validaters/converters.

        if (bindings == null) {
            return (null);
        }

        Object values[] = new Object[2];
        values[0] = bindings.keySet().toArray(new String[bindings.size()]);

        Object[] bindingValues = bindings.values().toArray();
        for (int i = 0; i < bindingValues.length; i++) {
            bindingValues[i] = UIComponentBase.saveAttachedState(context, bindingValues[i]);
        }

        values[1] = bindingValues;

        return (values);
    }

    // Utility for restoring bindings from state
    private static Map<String, ValueExpression> restoreBindings(FacesContext context,
                                                                Object state) {

        // Note: This code is copied from UIComponentBase.  See note above
        // in saveBindings().

        if (state == null) {
            return (null);
        }
        Object values[] = (Object[]) state;
        String names[] = (String[]) values[0];
        Object states[] = (Object[]) values[1];
        Map<String, ValueExpression> bindings = new HashMap<String, ValueExpression>(names.length);
        for (int i = 0; i < names.length; i++) {
            bindings.put(names[i],
                    (ValueExpression) UIComponentBase.restoreAttachedState(context, states[i]));
        }
        return (bindings);
    }

    @Override
    public String getScript(ClientBehaviorContext behaviorContext) {
        return "return jsf.bootstrap.confirm(this,'" +
                (behaviorContext.getComponent().getClientId() + "_confirm").replaceAll(":", "_") +
                "','" + getTitle() +
                "','" + getMessage() +
                "','" + getButtonYes() +
                "','" + getButtonNo() +
                "','" + getStyle() +
                "','" + getStyleClass() +
                "')";
    }

    public String getMessage() {
        return (String) eval(MESSAGE, message, Messages.CONFIRM_MESSAGE);
    }

    public void setMessage(String message) {
        this.message = message;
        clearInitialState();
    }

    public String getButtonYes() {
        return (String) eval(BUTTON_YES, buttonYes, Messages.CONFIRM_BUTTON_YES);
    }

    public void setButtonYes(String buttonYes) {
        this.buttonYes = buttonYes;
        clearInitialState();
    }

    public String getButtonNo() {
        return (String) eval(BUTTON_NO, buttonNo, Messages.CONFIRM_BUTTON_NO);
    }

    public void setButtonNo(String buttonNo) {
        this.buttonNo = buttonNo;
        clearInitialState();
    }

    public String getTitle() {
        return (String) eval(TITLE, title, Messages.CONFIRM_TITLE);
    }

    public void setTitle(String title) {
        this.title = title;
        clearInitialState();
    }

    public String getStyle() {
        return (String) eval(STYLE, style, null);
    }

    public void setStyle(String style) {
        this.style = style;
        clearInitialState();
    }

    public String getStyleClass() {
        return (String) eval(STYLE_CLASS, styleClass, null);
    }

    public void setStyleClass(String styleClass) {
        this.styleClass = styleClass;
        clearInitialState();
    }

//    @Override
//    public Object saveState(FacesContext context) {
//        Object[] values;
//
//        Object superState = super.saveState(context);
//
//        if (initialStateMarked()) {
//            if (superState == null) {
//                values = null;
//            } else {
//                values = new Object[]{superState};
//            }
//        } else {
//            values = new Object[8];
//
//            values[0] = superState;
//            values[1] = saveBindings(context, bindings);
//            values[2] = message;
//            values[3] = title;
//            values[4] = style;
//            values[5] = styleClass;
//            values[6] = buttonYes;
//            values[7] = buttonNo;
//        }
//        return values;
//    }
//
//    @Override
//    public void restoreState(FacesContext context, Object state) {
//        if (state != null) {
//
//            Object[] values = (Object[]) state;
//            super.restoreState(context, values[0]);
//
//            if (values.length != 1) {
//                bindings = restoreBindings(context, values[1]);
//                message = (String) values[2];
//                title = (String) values[3];
//                style = (String) values[4];
//                styleClass = (String) values[5];
//                buttonYes = (String) values[6];
//                buttonNo = (String) values[7];
//
//                // If we saved state last time, save state again next time.
//                clearInitialState();
//            }
//        }
//    }

    private Object eval(String propertyName, Object value, String defaultIdMessage) {

        if (value != null) {
            return value;
        }

        ValueExpression expression = getValueExpression(propertyName);

        if (expression != null) {
            FacesContext ctx = FacesContext.getCurrentInstance();
            return expression.getValue(ctx.getELContext());
        }

        return defaultIdMessage != null ? Messages.getMessage(defaultIdMessage) : null;
    }

    public ValueExpression getValueExpression(String name) {

        if (name == null) {
            throw new NullPointerException();
        }

        return ((bindings == null) ? null : bindings.get(name));
    }

    public void setValueExpression(String name, ValueExpression binding) {

        if (name == null) {
            throw new NullPointerException();
        }

        if (binding != null) {

            if (binding.isLiteralText()) {
                setLiteralValue(name, binding);
            } else {
                if (bindings == null) {

                    // We use a very small initial capacity on this HashMap.
                    // The goal is not to reduce collisions, but to keep the
                    // memory footprint small.  It is very unlikely that an
                    // an AjaxBehavior would have more than 1 or 2 bound
                    // properties - and even if more are present, it's okay
                    // if we have some collisions - will still be fast.
                    bindings = new HashMap<String, ValueExpression>(6, 1.0f);
                }

                bindings.put(name, binding);
            }
        } else {
            if (bindings != null) {
                bindings.remove(name);
                if (bindings.isEmpty()) {
                    bindings = null;
                }
            }
        }
        clearInitialState();
    }

    // Sets a property, converting it from a literal
    private void setLiteralValue(String propertyName,
                                 ValueExpression expression) {

        assert (expression.isLiteralText());

        Object value;
        ELContext context = FacesContext.getCurrentInstance().getELContext();

        try {
            value = expression.getValue(context);
        } catch (ELException ele) {
            throw new FacesException(ele);
        }

        if (MESSAGE.equals(propertyName)) {
            message = (String) value;
        }
        if (TITLE.equals(propertyName)) {
            title = (String) value;
        }
        if (STYLE.equals(propertyName)) {
            style = (String) value;
        }
        if (STYLE_CLASS.equals(propertyName)) {
            styleClass = (String) value;
        }
        if (BUTTON_YES.equals(propertyName)) {
            buttonYes = (String) value;
        }
        if (BUTTON_NO.equals(propertyName)) {
            buttonNo = (String) value;
        }
    }


}
