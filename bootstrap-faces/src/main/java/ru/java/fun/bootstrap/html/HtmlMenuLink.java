package ru.java.fun.bootstrap.html;

import ru.java.fun.bootstrap.ui.UIMenuLink;

import javax.faces.component.UIComponent;
import javax.faces.component.UIOutput;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import javax.faces.render.FacesRenderer;
import javax.faces.render.Renderer;
import java.io.IOException;

/**
 * @author Terentjev Dmitry
 *         Date: 09.01.14
 *         Time: 9:55
 */
@FacesRenderer(rendererType = "ru.java.fun.bootstrap.MenuLink", componentFamily = UIOutput.COMPONENT_FAMILY)
public class HtmlMenuLink extends Renderer {

    @Override
    public void encodeBegin(FacesContext context, UIComponent _component) throws IOException {
        ResponseWriter w = context.getResponseWriter();
        UIMenuLink c = (UIMenuLink) _component;
        w.startElement("li", c);
        w.writeAttribute("class", c.getStyleClass(), "");
        w.writeAttribute("style", c.getStyle(), "");
        w.writeAttribute("id", c.getClientId(), "");
        w.startElement("a", c);
        if (c.isDisabled()) {
            w.writeAttribute("href", "", "");
            w.writeAttribute("disabled", "disabled", "");
        } else {
            if (c.getTarget() != null && !c.getTarget().isEmpty())
                w.writeAttribute("target", c.getTarget(), "");
            w.writeAttribute("href", c.getValue().toString().contains("://") ? c.getValue().toString() : context.getExternalContext().getRequestContextPath() + c.getValue().toString(), "");
        }

    }

    @Override
    public boolean getRendersChildren() {
        return true;
    }

    @Override
    public void encodeEnd(FacesContext context, UIComponent uiComponent) throws IOException {
        ResponseWriter w = context.getResponseWriter();
        w.endElement("a");
        w.endElement("li");
    }
}
