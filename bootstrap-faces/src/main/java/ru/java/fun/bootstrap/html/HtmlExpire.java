package ru.java.fun.bootstrap.html;

import ru.java.fun.bootstrap.ResponseWriterExtended;
import ru.java.fun.bootstrap.ui.UIExpire;

import javax.faces.component.UIComponent;
import javax.faces.component.UIOutput;
import javax.faces.context.FacesContext;
import javax.faces.render.FacesRenderer;
import javax.faces.render.Renderer;
import java.io.IOException;

/**
 * @author Terentjev Dmitry
 *         Date: 09.01.14
 *         Time: 9:55
 */
@FacesRenderer(rendererType = "ru.java.fun.bootstrap.Expire", componentFamily = UIOutput.COMPONENT_FAMILY)
public class HtmlExpire extends Renderer {

    @Override
    public void encodeBegin(FacesContext context, UIComponent component) throws IOException {
        ResponseWriterExtended w = new ResponseWriterExtended(component, context);
        UIExpire c = (UIExpire) component;
        w.startElement("script");
        w.writeAttribute("type", "text/javascript");
        String id = c.getClientId().replaceAll(":", "\\\\\\\\:");
        w.writeText("$(function(){jsf.bootstrap.expire('" + id + "','" + c.getTitle() + "','" + c.getMessage() + "')})");
        w.endElement("script");
    }
}
