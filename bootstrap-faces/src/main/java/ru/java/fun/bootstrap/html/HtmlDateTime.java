package ru.java.fun.bootstrap.html;

import ru.java.fun.bootstrap.ui.UIDateTime;
import ru.java.fun.bootstrap.util.RenderKitUtils;

import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import javax.faces.render.FacesRenderer;
import javax.faces.render.Renderer;
import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * @author Terentjev Dmitry
 *         Date: 09.01.14
 *         Time: 9:55
 */
@FacesRenderer(rendererType = "ru.java.fun.bootstrap.DateTime", componentFamily = UIInput.COMPONENT_FAMILY)
public class HtmlDateTime extends Renderer {

    static Map<String, String> locales = new HashMap<String, String>();

    static {
        locales.put("de_AT", "de-at");
        locales.put("de", "de");
        locales.put("hi", "hi");
        locales.put("pt", "pt");
        locales.put("ms-MY", "ms-my");
        locales.put("lt", "lt");
        locales.put("hr", "hr");
        locales.put("lv", "lv");
        locales.put("tl_PH", "tl-ph");
        locales.put("pt_BR", "pt-br");
        locales.put("hu", "hu");
        locales.put("zh-CN", "zh-cn");
        locales.put("ar_SA", "ar-sa");
        locales.put("uk", "uk");
        locales.put("en-AU", "en-au");
        locales.put("hy-AM", "hy-am");
        locales.put("id", "id");
        locales.put("ar-TN", "ar-tn");
        locales.put("mk", "mk");
        locales.put("ml", "ml");
        locales.put("af", "af");
        locales.put("in", "id");
        locales.put("mr", "mr");
        locales.put("uz", "uz");
        locales.put("tzm", "tzm");
        locales.put("el", "el");
        locales.put("eo", "eo");
        locales.put("is", "is");
        locales.put("it", "it");
        locales.put("my", "my");
        locales.put("es", "es");
        locales.put("fr-CA", "fr-ca");
        locales.put("iw", "he");
        locales.put("et", "et");
        locales.put("eu", "eu");
        locales.put("ar", "ar");
        locales.put("vi", "vi");
        locales.put("nb", "nb");
        locales.put("ja", "ja");
        locales.put("ne", "ne");
        locales.put("az", "az");
        locales.put("ar-MA", "ar-ma");
        locales.put("fa", "fa");
        locales.put("ro", "ro");
        locales.put("en-GB", "en-gb");
        locales.put("nl", "nl");
        locales.put("nn", "nn");
        locales.put("be", "be");
        locales.put("en-CA", "en-ca");
        locales.put("fi", "fi");
        locales.put("ru", "ru");
        locales.put("ar_TN", "ar-tn");
        locales.put("bg", "bg");
        locales.put("en_AU", "en-au");
        locales.put("fo", "fo");
        locales.put("bn", "bn");
        locales.put("fr", "fr");
        locales.put("bo", "bo");
        locales.put("br", "br");
        locales.put("de-AT", "de-at");
        locales.put("ms_MY", "ms-my");
        locales.put("bs", "bs");
        locales.put("fy", "fy");
        locales.put("zh_TW", "zh-tw");
        locales.put("ka", "ka");
        locales.put("sk", "sk");
        locales.put("tzm_LATN", "tzm-latn");
        locales.put("sl", "sl");
        locales.put("ca", "ca");
        locales.put("sq", "sq");
        locales.put("sr_CYRL", "sr-cyrl");
        locales.put("sr", "sr");
        locales.put("ar-SA", "ar-sa");
        locales.put("km", "km");
        locales.put("fr_CA", "fr-ca");
        locales.put("sv", "sv");
        locales.put("ko", "ko");
        locales.put("gl", "gl");
        locales.put("zh-TW", "zh-tw");
        locales.put("pt-BR", "pt-br");
        locales.put("hy_AM", "hy-am");
        locales.put("ta", "ta");
        locales.put("ar_MA", "ar-ma");
        locales.put("cs", "cs");
        locales.put("cv", "cv");
        locales.put("en_GB", "en-gb");
        locales.put("th", "th");
        locales.put("cy", "cy");
        locales.put("en_CA", "en-ca");
        locales.put("lb", "lb");
        locales.put("tl-PH", "tl-ph");
        locales.put("zh_CN", "zh-cn");
        locales.put("pl", "pl");
        locales.put("da", "da");
        locales.put("he", "he");
        locales.put("tr", "tr");
    }

    @Override
    public void encodeBegin(FacesContext context, UIComponent _component) throws IOException {
        ResponseWriter w = context.getResponseWriter();
        UIDateTime c = (UIDateTime) _component;
        if (c.getConverter() == null) {
            c.initConverter();
        }
        w.startElement("div", c);
        w.writeAttribute("class", "input-group date", "");
        w.writeAttribute("id", c.getClientId(), "");
        w.startElement("input", c);
        w.writeAttribute("type", "text", "");
        w.writeAttribute("class", "form-control " + c.getStyleClass(), "");
        if (c.getStyle() != null)
            w.writeAttribute("style", c.getStyle(), "");
        if (c.isDisabled())
            w.writeAttribute("disabled", "disabled", "");
        w.writeAttribute("id", c.getClientId() + ":date", "");
        w.writeAttribute("name", c.getClientId() + ":date", "");
        w.writeAttribute("value", c.getConverter().getAsString(context, c, c.getValue()), "");
    }

    @Override
    public boolean getRendersChildren() {
        return true;
    }

    @Override
    public void encodeEnd(FacesContext context, UIComponent uiComponent) throws IOException {
        ResponseWriter w = context.getResponseWriter();
        w.endElement("input");
        w.startElement("span", uiComponent);
        w.writeAttribute("class", "input-group-addon", "");
        w.startElement("span", uiComponent);
        w.writeAttribute("class", "glyphicon glyphicon-calendar", "");
        w.endElement("span");
        w.endElement("span");
        w.endElement("div");
        UIDateTime c = (UIDateTime) uiComponent;
        w.startElement("script", c);
        w.writeAttribute("type", "text/javascript", "");
        String _id = "#" + c.getClientId().replaceAll(":", "\\\\\\\\:") + "";
        String id = "'" + _id + "'";
        String jf = javascriptFormat(c.getPattern());
        String dis;
        if (c.isDisabled()) {
            dis = "$('" + _id + ">span').css('cursor','not-allowed').unbind('click');";
        } else {
            dis = "$('" + _id + ">span').css('cursor','pointer');";
        }
        String change = RenderKitUtils.scriptHandler(context, c, null, "change", c.getOnchange(), "");
        String show = RenderKitUtils.scriptHandler(context, c, null, "show", c.getOnshow(), "");
        String hide = RenderKitUtils.scriptHandler(context, c, null, "hide", c.getOnhide(), "");
        StringBuilder events = new StringBuilder();
        chain(change, "dp.change", events);
        chain(show, "dp.show", events);
        chain(hide, "dp.hide", events);
        String options = String.format("{format: '%s',showTodayButton:true,locale:'%s'}", jf, getLocale(context.getViewRoot().getLocale()));
        w.writeText("$(function(){\n$(" + id + ").datetimepicker(" + options + ")" + events.toString() + ";" + dis + "}\n)", "");
        w.endElement("script");
    }

    private void chain(String script, String name, StringBuilder b) {
        if (!script.isEmpty()) {
            b.append(".on('").append(name).append("',function(event){").append(script).append("})");
        }
    }

    private String javascriptFormat(String f) {
        return f.replaceAll("k", "H").replaceAll("K", "h").replaceAll("D", "k").replaceAll("d", "D")
                .replaceAll("k", "DDD").replaceAll("W", "").replaceAll("F", "e").replaceAll("EEEE", "dddd")
                .replaceAll("EEE", "ddd").replaceAll("y", "Y");

    }

    private String getLocale(Locale locale) {
        if (locales.containsKey(locale.toString())) {
            return locales.get(locale.toString());
        }
        if (locales.containsKey(locale.toLanguageTag())) {
            return locales.get(locale.toLanguageTag());
        }
        return "en";
    }

}
