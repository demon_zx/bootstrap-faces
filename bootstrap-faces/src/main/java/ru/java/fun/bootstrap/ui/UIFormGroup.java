package ru.java.fun.bootstrap.ui;

import javax.faces.component.FacesComponent;
import javax.faces.component.UIPanel;

/**
 * @author Terentjev Dmitry
 *         Date: 11.12.13
 *         Time: 10:09
 */
@FacesComponent("formGroup")
public class UIFormGroup extends UIPanel {

    @Override
    public String getRendererType() {
        return "ru.java.fun.bootstrap.FormGroup";
    }

    public String getStyle() {
        return (String) getStateHelper().eval("style");
    }

    public void setStyle(String style) {
        getStateHelper().put("style",style);
    }

    public String getStyleClass() {
        return (String) getStateHelper().eval("styleClass","");
    }

    public void setStyleClass(String styleClass) {
        getStateHelper().put("styleClass",styleClass);
    }

    public String getColumns() {
        return (String) getStateHelper().eval("columns");
    }

    public void setColumns(String columns) {
        getStateHelper().put("columns",columns);
    }

    public String getOffsets() {
        return (String) getStateHelper().eval("offsets");
    }

    public void setOffsets(String offsets) {
        getStateHelper().put("offsets",offsets);
    }
}
