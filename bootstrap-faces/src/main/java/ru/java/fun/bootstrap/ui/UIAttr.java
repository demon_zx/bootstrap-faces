package ru.java.fun.bootstrap.ui;

import javax.faces.application.ResourceDependencies;
import javax.faces.application.ResourceDependency;
import javax.faces.component.FacesComponent;
import javax.faces.component.UIOutput;

/**
 * @author Terentjev Dmitry
 *         Date: 11.12.13
 *         Time: 10:09
 */
@FacesComponent("attr")
@ResourceDependencies({
        @ResourceDependency(library = "bootstrap/js", name = "jquery.min.js", target = "head")
})
public class UIAttr extends UIOutput {

    @Override
    public String getRendererType() {
        return "ru.java.fun.bootstrap.Attr";
    }

    public String getFor() {
        return (String) getStateHelper().eval("for");
    }

    public void setFor(String forId) {
        getStateHelper().put("for",forId);
    }

    public String getSelector() {
        return (String) getStateHelper().eval("selector");
    }

    public void setSelector(String selector) {
        getStateHelper().put("selector",selector);
    }

    public String getName() {
        return (String) getStateHelper().eval("name");
    }

    public void setName(String name) {
        getStateHelper().put("name",name);
    }
}
