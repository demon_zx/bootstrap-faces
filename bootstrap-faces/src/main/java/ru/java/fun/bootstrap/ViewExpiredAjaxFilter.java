package ru.java.fun.bootstrap;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;

/**
 * @author Terentjev Dmitry
 *         Date: 03.06.2014
 *         Time: 13:46
 */
public class ViewExpiredAjaxFilter implements Filter {

    private final String message = "<?xml version='1.0' encoding='UTF-8'?>\n" +
            "<partial-response>\n" +
            "<error>\n" +
            "<error-name>class javax.faces.application.ViewExpiredException</error-name>\n" +
            "<error-message><![CDATA[\n" +
            "viewId:%s - View %s could not be restored.\n" +
            "]]></error-message>\n" +
            "</error></partial-response>";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        if ("partial/ajax".equals(request.getHeader("Faces-Request")) && request.getSession().isNew()) {
            HttpServletResponse resp = (HttpServletResponse) servletResponse;
            resp.reset();
            resp.setContentType("text/xml");
            resp.getWriter().printf(message, request.getRequestURI(), request.getRequestURI());
            resp.setStatus(HttpServletResponse.SC_OK);
            return;
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}
