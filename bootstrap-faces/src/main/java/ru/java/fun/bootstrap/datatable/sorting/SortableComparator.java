package ru.java.fun.bootstrap.datatable.sorting;

import java.util.Comparator;

/**
 * @author Terentjev Dmitry
 *         Date: 03.02.14
 *         Time: 13:21
 */
public interface SortableComparator<T> extends Comparator<T> {

    public SortableState getState();

    public void setState(SortableState state);

    public void nextState();

    public String getFieldName();

}
