package ru.java.fun.bootstrap.ui;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItem;

import javax.faces.FacesException;
import javax.faces.application.ResourceDependencies;
import javax.faces.application.ResourceDependency;
import javax.faces.component.FacesComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Terentjev Dmitry
 *         Date: 11.12.13
 *         Time: 10:09
 */
@FacesComponent("inputFile")
@ResourceDependencies({
        @ResourceDependency(name = "bootstrap/css/bootstrap.min.css", target = "head"),
        @ResourceDependency(name = "bootstrap/css/bootstrap-theme.min.css", target = "head")
//        @ResourceDependency(name = "bootstrap/css/datepicker.css", target = "head")
//        @ResourceDependency(library = "bootstrap/js", name = "jquery.min.js", target = "head"),
//        @ResourceDependency(library = "bootstrap/js", name = "bootstrap.min.js", target = "head"),
})
public class UIInputFile extends UIInput {

    @Override
    public String getRendererType() {
        return "ru.java.fun.bootstrap.InputFile";
    }

    public boolean isDisabled() {
        return (Boolean) getStateHelper().eval("disabled", false);
    }

    public void setDisabled(boolean disabled) {
        getStateHelper().put("disabled", disabled);
    }

    public boolean isMultiple() {
        return (Boolean) getStateHelper().eval("multiple", false);
    }

    public void setMultiple(boolean v) {
        getStateHelper().put("multiple", v);
    }

    public String getStyle() {
        return (String) getStateHelper().eval("style");
    }

    public void setStyle(String style) {
        getStateHelper().put("style", style);
    }

    public String getStyleClass() {
        return (String) getStateHelper().eval("styleClass", "");
    }

    public void setStyleClass(String styleClass) {
        getStateHelper().put("styleClass", styleClass);
    }

    public String getAccept() {
        return (String) getStateHelper().eval("accept");
    }

    public void setAccept(String accept) {
        getStateHelper().put("accept", accept);
    }

    @Override
    public void decode(FacesContext context) {
        //todo decode behaviors

//        File file = ((MultipartRequest)context.getExternalContext().getRequest()).getFile(getClientId());

        Map<String, String> pm = context.getExternalContext().getRequestParameterMap();
        if (pm.containsKey(getClientId())) {
            Class cl = getValueExpression("value").getType(context.getELContext());
            Type type = getType(cl);
            if (isMultiple()) {
                int count = Integer.parseInt(pm.get(getClientId()));
                List<File> files = new ArrayList<File>();
                for (int i = 0; i < count; i++) {
                    if (isFileCorrect(pm, getClientId() + ":" + i)) {
                        File fi = new File(pm.get(getClientId() + ":" + i));
                        if (fi.exists())
                            files.add(fi);
                    }
                }
                setSubmittedValue(files);
            } else {
                if (isFileCorrect(pm, getClientId())) {
                    FileItem fi = (FileItem) ((HttpServletRequest) context.getExternalContext().getRequest()).getAttribute(getClientId() + ":file:0");
                    DiskFileItem di = null;
                    if (fi instanceof DiskFileItem) {
                        di = (DiskFileItem) fi;
                    }
                    switch (type) {
                        case FILE_ITEM:
                            setSubmittedValue(fi);
                            break;
                        case FILE:
                            if (di != null) {
                                File file = di.getStoreLocation();
                                if (file.exists()) {
                                    setSubmittedValue(fi);
                                }
                            }
                            break;
                        case BYTES:
                            if(di!=null){
                                if(di.isInMemory()){
                                    setSubmittedValue(di.get());
                                }else{
                                    ByteArrayOutputStream bos = new ByteArrayOutputStream((int) di.getSize());
                                    byte[] buf = new byte[1024*128];
                                    int rb;
                                    try {
                                        InputStream is = di.getInputStream();
                                        while ((rb=is.read(buf))!=-1){
                                            bos.write(buf,0,rb);
                                        }
                                        is.close();
                                        bos.flush();
                                        setSubmittedValue(bos.toByteArray());
                                    } catch (IOException e) {
                                        throw new FacesException(e);
                                    }
                                }
                            }
                    }
                }
            }
        }


    }

    private boolean isFileCorrect(Map<String, String> pm, String id) {
        String nm = pm.get(getClientId());
        return !nm.trim().isEmpty() && !"undefine".equals(nm);
    }

    private Type getType(Class clz) {
        if (byte[].class == clz)
            return Type.BYTES;
        if (File.class == clz)
            return Type.FILE;
        if (FileItem.class == clz)
            return Type.FILE_ITEM;
        return Type.NOT_SUPPORTED;
    }

    enum Type {
        FILE, FILE_ITEM, BYTES, NOT_SUPPORTED
    }
}
