package ru.java.fun.bootstrap.html;

import ru.java.fun.bootstrap.ui.UIAccordion;

import javax.faces.component.UIComponent;
import javax.faces.component.UIPanel;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import javax.faces.render.FacesRenderer;
import javax.faces.render.Renderer;
import java.io.IOException;

/**
 * @author Terentjev Dmitry
 *         Date: 09.01.14
 *         Time: 9:55
 */
@FacesRenderer(rendererType = "ru.java.fun.bootstrap.Accordion", componentFamily = UIPanel.COMPONENT_FAMILY)
public class HtmlAccordion extends Renderer {

    @Override
    public void encodeBegin(FacesContext context, UIComponent _component) throws IOException {
        ResponseWriter w = context.getResponseWriter();
        UIAccordion c = (UIAccordion) _component;
        w.startElement("div", c);
        w.writeAttribute("class", "panel-group " + c.getStyleClass(), "");
        if (c.getStyle() != null)
            w.writeAttribute("style", c.getStyle(), "");
        w.writeAttribute("id", c.getClientId(), "");
    }

    @Override
    public boolean getRendersChildren() {
        return true;
    }

    @Override
    public void encodeEnd(FacesContext context, UIComponent uiComponent) throws IOException {
        ResponseWriter w = context.getResponseWriter();
        w.endElement("div");
    }
}
