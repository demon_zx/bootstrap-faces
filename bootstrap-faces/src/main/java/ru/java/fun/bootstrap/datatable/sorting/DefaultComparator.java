package ru.java.fun.bootstrap.datatable.sorting;

/**
 * @author Terentjev Dmitry
 *         Date: 03.02.14
 *         Time: 13:18
 */
public abstract class DefaultComparator<T> implements SortableComparator<T> {

    private SortableState state = SortableState.unsorted;

    @Override
    public SortableState getState() {
        return state;
    }

    @Override
    public void setState(SortableState state) {
        this.state = state;
    }

    @Override
    public void nextState() {
        if (state == SortableState.unsorted || state == SortableState.descending) {
            state = SortableState.ascending;
        } else {
            state = SortableState.descending;
        }
    }

}
