package ru.java.fun.bootstrap.html;

import ru.java.fun.bootstrap.ResponseWriterExtended;
import ru.java.fun.bootstrap.ui.UIPanel;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.render.FacesRenderer;
import javax.faces.render.Renderer;
import java.io.IOException;

/**
 * @author Terentjev Dmitry
 *         Date: 09.01.14
 *         Time: 9:55
 */
@FacesRenderer(rendererType = "ru.java.fun.bootstrap.Panel", componentFamily = javax.faces.component.UIPanel.COMPONENT_FAMILY)
public class HtmlPanel extends Renderer {

    @Override
    public void encodeBegin(FacesContext context, UIComponent _component) throws IOException {
        ResponseWriterExtended w = new ResponseWriterExtended(_component, context.getResponseWriter());
        UIPanel c = (UIPanel) _component;
        w.startElement("div");
        w.writeAttribute("class", "panel panel-" + c.getContext() + " " + c.getStyleClass());
        if (c.getStyle() != null)
            w.writeAttribute("style", c.getStyle());
        w.writeAttribute("id", c.getClientId());

        UIComponent facet = c.getFacet("header");
        if (facet != null || c.getTitle() != null) {
            w.startElement("div");
            w.writeAttribute("class", "panel-heading");
            if (c.getTitle() != null) {
                w.startElement("h3");
                w.writeAttribute("class", "panel-title");
                w.writeText(c.getTitle());
                w.endElement("h3");
            } else {
                if (facet != null) {
                    facet.encodeAll(context);
                }
            }
            w.endElement("div");
        }
        w.startElement("div");
        w.writeAttribute("class", "panel-body");
    }

    @Override
    public boolean getRendersChildren() {
        return true;
    }

    @Override
    public void encodeEnd(FacesContext context, UIComponent uiComponent) throws IOException {
        UIPanel c = (UIPanel) uiComponent;
        ResponseWriterExtended w = new ResponseWriterExtended(c, context.getResponseWriter());
        UIComponent facet = c.getFacet("footer");
        if (facet != null) {
            w.startElement("div");
            w.writeAttribute("class", "panel-footer");
            facet.encodeAll(context);
            w.endElement("div");
        }
        w.endElement("div");
        w.endElement("div");
    }
}
