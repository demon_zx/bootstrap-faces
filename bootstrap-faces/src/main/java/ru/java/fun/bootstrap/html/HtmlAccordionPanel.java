package ru.java.fun.bootstrap.html;

import ru.java.fun.bootstrap.ui.UIAccordionPanel;

import javax.faces.component.UIComponent;
import javax.faces.component.UIPanel;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import javax.faces.render.FacesRenderer;
import javax.faces.render.Renderer;
import java.io.IOException;

/**
 * @author Terentjev Dmitry
 *         Date: 09.01.14
 *         Time: 9:55
 */
@FacesRenderer(rendererType = "ru.java.fun.bootstrap.AccordionPanel", componentFamily = UIPanel.COMPONENT_FAMILY)
public class HtmlAccordionPanel extends Renderer {

    @Override
    public void encodeBegin(FacesContext context, UIComponent _component) throws IOException {
        ResponseWriter w = context.getResponseWriter();
        UIAccordionPanel c = (UIAccordionPanel) _component;
        w.startElement("div", c);
        w.writeAttribute("class", "panel panel-default " + c.getStyleClass(), "");
        if (c.getStyle() != null)
            w.writeAttribute("style", c.getStyle(), "");
        w.writeAttribute("id", c.getClientId(), "");
        w.startElement("div", c);
        w.writeAttribute("class", "panel-heading", "");
        w.startElement("h3", c);
        w.writeAttribute("class", "panel-title", "");
        w.startElement("a", c);
        w.writeAttribute("data-toggle", "collapse", "");
        w.writeAttribute("data-parent", "#" + c.getParent().getClientId(), "");
        w.writeAttribute("href", "#" + c.getClientId().replaceAll("\\:", "\\\\:") + "\\:cp", "");
        w.writeText(c.getTitle(), "");
        w.endElement("a");
        w.endElement("h3");
        w.endElement("div");
        w.startElement("div", c);
        w.writeAttribute("class", "panel-collapse collapse " + (c.isOpen() ? "in" : ""), "");
        w.writeAttribute("id", c.getClientId() + ":cp", "");
        w.startElement("div", c);
        w.writeAttribute("class", "panel-body", "");
        w.writeAttribute("id", c.getClientId() + ":body", "");
    }

    @Override
    public boolean getRendersChildren() {
        return true;
    }

    @Override
    public void encodeEnd(FacesContext context, UIComponent uiComponent) throws IOException {
        ResponseWriter w = context.getResponseWriter();
        w.endElement("div");
        w.endElement("div");
        w.endElement("div");
    }
}
