package ru.java.fun.bootstrap.util;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

/**
 * @author Terentjev Dmitry
 *         Date: 25.02.14
 *         Time: 12:58
 */
public class URLBuilder {

    String url;

    public URLBuilder(String url) {
        this.url = url;
    }

    public URLBuilder addParameter(String name, String value) {
        if (!url.contains("?"))
            url += "?";
        url += name + "=" + value + "&";
        return this;
    }

    public URLBuilder addParameterEncoded(String name, String value) {
        try {
            return addParameter(name, URLEncoder.encode(value, "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            return this;
        }
    }

    public String buildString() {
        return url;
    }

    public URL build() throws MalformedURLException {
        return new URL(url);
    }
}
