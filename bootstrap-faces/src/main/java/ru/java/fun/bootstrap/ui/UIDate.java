package ru.java.fun.bootstrap.ui;

import javax.faces.application.ResourceDependencies;
import javax.faces.application.ResourceDependency;
import javax.faces.component.FacesComponent;
import javax.faces.component.UIInput;
import javax.faces.component.behavior.ClientBehavior;
import javax.faces.component.behavior.ClientBehaviorHolder;
import javax.faces.context.FacesContext;
import javax.faces.convert.DateTimeConverter;
import java.util.*;

/**
 * @author Terentjev Dmitry
 *         Date: 11.12.13
 *         Time: 10:09
 */
@FacesComponent("date")
@ResourceDependencies({
        @ResourceDependency(name = "bootstrap/css/bootstrap.min.css", target = "head"),
        @ResourceDependency(name = "bootstrap/css/bootstrap-theme.min.css", target = "head"),
        @ResourceDependency(name = "bootstrap/css/datepicker.css", target = "head"),
        @ResourceDependency(library = "bootstrap/js", name = "jquery.min.js", target = "head"),
        @ResourceDependency(library = "javax.faces", name = "jsf.js"),
        @ResourceDependency(library = "bootstrap/js", name = "bootstrap.min.js", target = "head"),
        @ResourceDependency(library = "bootstrap/js", name = "jsf-bs.js", target = "head"),
        @ResourceDependency(library = "bootstrap/js", name = "bootstrap-datepicker.js", target = "head"),
        @ResourceDependency(library = "bootstrap/js", name = "bootstrap-datepicker.ru.js", target = "head")
})
public class UIDate extends UIInput implements ClientBehaviorHolder {
    protected static final String DEFAULT_EVENT = "change";
    protected static final Collection<String> EVENT_NAMES = Collections.unmodifiableCollection(Arrays.asList(DEFAULT_EVENT, "show", "hide"));

    @Override
    public Collection<String> getEventNames() {
        return EVENT_NAMES;
    }

    @Override
    public String getDefaultEventName() {
        return DEFAULT_EVENT;
    }

    @Override
    public String getRendererType() {
        return "ru.java.fun.bootstrap.Date";
    }

    public boolean isDisabled() {
        return (Boolean) getStateHelper().eval("disabled", false);
    }

    public void setDisabled(boolean disabled) {
        getStateHelper().put("disabled", disabled);
    }

    public String getStyle() {
        return (String) getStateHelper().eval("style");
    }

    public void setStyle(String style) {
        getStateHelper().put("style", style);
    }

    public String getStyleClass() {
        return (String) getStateHelper().eval("styleClass", "");
    }

    public void setStyleClass(String styleClass) {
        getStateHelper().put("styleClass", styleClass);
    }

    public String getPattern() {
        return (String) getStateHelper().eval("pattern", "dd.MM.yyyy");
    }

    public void setPattern(String pattern) {
        getStateHelper().put("pattern", pattern);
    }

    public void initConverter() {
        DateTimeConverter c = new DateTimeConverter();
        c.setPattern(getPattern());
        c.setTimeZone(TimeZone.getTimeZone(getTimeZone()));
        setConverter(c);
    }

    public String getTimeZone() {
        return (String) getStateHelper().eval("timeZone", TimeZone.getDefault().getID());
    }

    public void setTimeZone(String timeZone) {
        getStateHelper().put("timeZone", timeZone);
    }

    public String getOnchange() {
        return (String) getStateHelper().eval("onchange");
    }

    public void setOnchange(String onchange) {
        getStateHelper().put("onchange",onchange);
    }

    public String getOnshow() {
        return (String) getStateHelper().eval("onshow");
    }

    public void setOnshow(String onshow) {
        getStateHelper().put("onshow",onshow);
    }

    public String getOnhide() {
        return (String) getStateHelper().eval("onhide");
    }

    public void setOnhide(String onhide) {
        getStateHelper().put("onhide",onhide);
    }

    @Override
    public void decode(FacesContext context) {
        if(isDisabled()){
            return;
        }
        if (getConverter() == null) {
            initConverter();
        }
        String val = context.getExternalContext().getRequestParameterMap().get(getClientId() + ":date");
        setSubmittedValue(getConverter().getAsObject(context, this, val));
        setValid(true);

        decodeBehaviors(context);
    }

    protected void decodeBehaviors(FacesContext context) {
        Map<String, String> pars = context.getExternalContext().getRequestParameterMap();
        Map<String, List<ClientBehavior>> mapBeh = getClientBehaviors();
        String event = pars.get("javax.faces.behavior.event");
        List<ClientBehavior> beh = mapBeh.get(event);
        if (beh != null && !beh.isEmpty()) {
            String source = pars.get("javax.faces.source");
            String clientId = getClientId(context);
            if (clientId.equals(source)) {
                for (ClientBehavior cb : beh) {
                    cb.decode(context, this);
                }
            }
        }
    }
}
