package ru.java.fun.bootstrap.ui;

import javax.faces.component.FacesComponent;
import javax.faces.component.UIOutput;

/**
 * @author Terentjev Dmitry
 *         Date: 11.12.13
 *         Time: 10:09
 */
@FacesComponent("json")
public class UIJson extends UIOutput {

    @Override
    public String getRendererType() {
        return "ru.java.fun.bootstrap.Json";
    }

    public String getVarName() {
        return (String) getStateHelper().eval("varName");
    }

    public void setVarName(String varName) {
        getStateHelper().put("varName",varName);
    }
}
