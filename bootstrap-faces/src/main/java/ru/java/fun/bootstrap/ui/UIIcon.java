package ru.java.fun.bootstrap.ui;

import javax.faces.application.ResourceDependencies;
import javax.faces.application.ResourceDependency;
import javax.faces.component.FacesComponent;

/**
 * @author Terentjev Dmitry
 *         Date: 11.12.13
 *         Time: 10:09
 */
@FacesComponent("icon")
@ResourceDependencies({
        @ResourceDependency(name = "bootstrap/css/bootstrap.min.css", target = "head"),
        @ResourceDependency(name = "bootstrap/css/bootstrap-theme.min.css", target = "head")
})
public class UIIcon extends UIStyledOutput {

    @Override
    public String getRendererType() {
        return "ru.java.fun.bootstrap.Icon";
    }

    public String getTitle() {
        return (String) getStateHelper().eval("title");
    }

    public void setTitle(String title) {
        getStateHelper().put("title", title);
    }

    public String getName() {
        return (String) getStateHelper().eval("name");
    }

    public void setName(String icon) {
        getStateHelper().put("name", icon);
    }
}
