package ru.java.fun.bootstrap.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Dmitry on 10.12.13.
 * хелпер для работы с датами
 */
public class DateBuilder {

    private Calendar date;

    public DateBuilder(Date date) {
        if (date != null) {
            this.date = Calendar.getInstance();
            this.date.setTime(date);
        } else {
            this.date = null;
        }
    }

    public DateBuilder() {
        this(new Date());
    }

    public static DateBuilder init(Date date) {
        return new DateBuilder(date);
    }

    public static DateBuilder init(String date, String format) {
        try {
            return new DateBuilder(new SimpleDateFormat(format).parse(date));
        } catch (ParseException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public static DateBuilder init() {
        return new DateBuilder();
    }

    public Date build() {
        if (date != null)
            return date.getTime();
        return null;
    }

    public DateBuilder wrapToSecond() {
        if (date != null)
            date.set(Calendar.MILLISECOND, 0);
        return this;
    }

    public int second() {
        if (date != null)
            return date.get(Calendar.SECOND);
        return 0;
    }

    public int minute() {
        if (date != null)
            return date.get(Calendar.MINUTE);
        return 0;
    }

    public int minuteOfDay() {
        if (date != null)
            return minute() + date.get(Calendar.HOUR_OF_DAY) * 60;
        return 0;
    }

    public int secondOfHour() {
        if (date != null)
            return second() + minute() * 60;
        return 0;
    }

    public int secondOfDay() {
        if (date != null)
            return second() + minuteOfDay() * 60;
        return 0;
    }

    public int hour() {
        if (date != null)
            return date.get(Calendar.HOUR_OF_DAY);
        return 0;
    }

    public DateBuilder wrapToMinute() {
        wrapToSecond();
        if (date != null)
            date.set(Calendar.SECOND, 0);
        return this;
    }

    public DateBuilder wrapToHour() {
        wrapToMinute();
        if (date != null)
            date.set(Calendar.MINUTE, 0);
        return this;
    }

    public DateBuilder wrapToDay() {
        wrapToHour();
        if (date != null)
            date.set(Calendar.HOUR_OF_DAY, 0);
        return this;
    }

    public DateBuilder wrapToMonth() {
        wrapToDay();
        if (date != null)
            date.set(Calendar.DAY_OF_MONTH, 1);
        return this;
    }

    public DateBuilder addHour(int hours) {
        if (date != null)
            date.add(Calendar.HOUR, hours);
        return this;
    }

    public DateBuilder addHour() {
        return addHour(1);
    }

    public DateBuilder addDay(int days) {
        if (date != null)
            date.add(Calendar.DAY_OF_YEAR, days);
        return this;
    }

    public DateBuilder addDay() {
        return addDay(1);
    }

    public DateBuilder addSecond(int seconds) {
        if (date != null)
            date.add(Calendar.SECOND, seconds);
        return this;
    }

    public DateBuilder addSecond() {
        return addSecond(1);
    }

    public DateBuilder addMonth(int months) {
        if (date != null)
            date.add(Calendar.MONTH, months);
        return this;
    }

    public DateBuilder addMonth() {
        return addMonth(1);
    }

    public DateBuilder addMinute(int minutes) {
        if (date != null)
            date.add(Calendar.MINUTE, minutes);
        return this;
    }

    public DateBuilder addMinute() {
        return addMinute(1);
    }

    public DateBuilder duplicate() {
        return DateBuilder.init(build());
    }

    public DateBuilder applyDate(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        if (this.date != null) {
            this.date.set(Calendar.YEAR, c.get(Calendar.YEAR));
            this.date.set(Calendar.MONTH, c.get(Calendar.MONTH));
            this.date.set(Calendar.DAY_OF_YEAR, c.get(Calendar.DAY_OF_YEAR));
        }
        return this;
    }

    public DateBuilder applyTime(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        if (this.date != null) {
            this.date.set(Calendar.HOUR, c.get(Calendar.HOUR_OF_DAY));
            this.date.set(Calendar.MINUTE, c.get(Calendar.MINUTE));
            this.date.set(Calendar.SECOND, c.get(Calendar.SECOND));
            this.date.set(Calendar.MILLISECOND, c.get(Calendar.MILLISECOND));
        }
        return this;
    }

}
