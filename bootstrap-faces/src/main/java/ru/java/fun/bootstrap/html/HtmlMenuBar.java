package ru.java.fun.bootstrap.html;

import ru.java.fun.bootstrap.ResponseWriterExtended;
import ru.java.fun.bootstrap.ui.UIMenuBar;

import javax.faces.component.UIComponent;
import javax.faces.component.UIPanel;
import javax.faces.context.FacesContext;
import javax.faces.render.FacesRenderer;
import javax.faces.render.Renderer;
import java.io.IOException;

/**
 * @author Terentjev Dmitry
 *         Date: 09.01.14
 *         Time: 9:55
 */
@FacesRenderer(rendererType = "ru.java.fun.bootstrap.MenuBar", componentFamily = UIPanel.COMPONENT_FAMILY)
public class HtmlMenuBar extends Renderer {

    @Override
    public void encodeBegin(FacesContext context, UIComponent _component) throws IOException {
        ResponseWriterExtended w = new ResponseWriterExtended(_component, context.getResponseWriter());
        UIMenuBar c = (UIMenuBar) _component;
        w.startElement("div");
        boolean inv = c.isInverse();
        boolean fix = c.isFixed();
        boolean top = c.isTop();
        w.writeAttribute("class", "navbar navbar-" + (inv ? "inverse" : "default") +
                " navbar-" + (fix ? "fixed" : "static") + (top ? "-top" : "-bottom")
                + c.getStyleClass());
        w.writeAttribute("style", c.getStyle());
        w.writeAttribute("role", "navigation");
        w.writeAttribute("id", c.getClientId());
        w.writeAttribute("data-top-padding", c.getContentPadding());
        if (c.getBrand() != null) {
            w.startElement("div");
            w.writeAttribute("class", "navbar-header");
            w.startElement("div");
            w.writeAttribute("class", "navbar-brand");
            String bt = c.getBrandTitle();
            if (bt != null && !bt.isEmpty())
                w.writeAttribute("title", c.getBrandTitle());
            w.writeText(c.getBrand());
            w.endElement("div");
            w.endElement("div");
        }
    }

    @Override
    public boolean getRendersChildren() {
        return true;
    }

    @Override
    public void encodeEnd(FacesContext context, UIComponent uiComponent) throws IOException {
        ResponseWriterExtended w = new ResponseWriterExtended(uiComponent, context.getResponseWriter());
        w.endElement("div");
        UIMenuBar c = (UIMenuBar) uiComponent;
        if (c.isTop()) {
            w.startElement("script");
            w.writeAttribute("type", "text/javascript");
            String id = c.getClientId().replaceAll(":", "\\\\\\\\:");
            w.writeText("$(function(){\nvar f = function(){jsf.bootstrap.menuInit('#" + id + "')};\nf();\n$(window).resize(f)}\n)");
            w.endElement("script");
        }
    }
}
