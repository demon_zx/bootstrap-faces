package ru.java.fun.bootstrap;

import javax.faces.application.ResourceHandler;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * @author Terentjev Dmitry
 *         Date: 12.12.13
 *         Time: 15:26
 */
@WebFilter(urlPatterns = {"*.png","*.gif","*.woff","*.ttf","*.svg"})
public class ResourceReWriter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest r = (HttpServletRequest) servletRequest;
        if (r.getServletPath().startsWith(ResourceHandler.RESOURCE_IDENTIFIER)) {
            r.getRequestDispatcher(r.getServletPath() + ".jsf"+(r.getRequestURI().contains(ResourceHandler.RESOURCE_IDENTIFIER+"/bootstrap")?"":"?ln=bootstrap")).forward(servletRequest, servletResponse);
        } else {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    @Override
    public void destroy() {

    }
}
