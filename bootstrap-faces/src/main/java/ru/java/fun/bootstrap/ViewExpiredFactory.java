package ru.java.fun.bootstrap;

import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerFactory;

/**
 * @author Terentjev Dmitry
 *         Date: 27.12.12
 *         Time: 15:30
 */
public class ViewExpiredFactory extends ExceptionHandlerFactory {

    private ExceptionHandlerFactory parent;

    public ViewExpiredFactory(ExceptionHandlerFactory parent) {
        this.parent = parent;
    }

    @Override
    public ExceptionHandler getExceptionHandler() {
        return new ViewExpiredHandler(parent.getExceptionHandler());
    }
}
