package ru.java.fun.bootstrap.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.io.support.ResourcePropertySource;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.ServletContext;
import java.io.IOException;

/**
 * @author Terentjev Dmitry
 *         Date: 24.08.12
 *         Time: 11:03
 */
public class Version implements ApplicationContextAware {

    public static final String APPLICATION_VERSION = "application.version";
    public static final String APPLICATION_VERSION_YEAR = "application.version.year";
    public static final String APPLICATION_VERSION_DATE = "application.version.date";
    private String version;
    private String versionDate, versionYear;

    public Version(String name) {
        setVersionPropertiesFileName(name);
    }

    public Version() {
        super();
    }

    public void setVersionPropertiesFileName(String name) {
        try {
            ResourcePropertySource prs = new ResourcePropertySource("classpath:/" + name);
            version = (String) prs.getProperty("app.version");
            String tmp = (String) prs.getProperty("app.datebuild");
            versionDate = tmp != null && tmp.length() > 8 ? tmp.substring(2, 8) : "120102";
            versionYear = tmp != null && tmp.length() > 4 ? tmp.substring(0, 4) : "2012";
        } catch (IOException e) {
            e.printStackTrace();
            version = "1.xx";
            versionDate = "120101";
            versionYear = "2012";
        }
    }

    public String getVersion() {
        return version;
    }

    public String getVersionDate() {
        return versionDate;
    }

    public String getVersionYear() {
        return versionYear;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        WebApplicationContext ctx = (WebApplicationContext) applicationContext;
        ServletContext servletContext = ctx.getServletContext();
        servletContext.setAttribute("version", version);
        servletContext.setAttribute("versionYear", versionYear);
        servletContext.setAttribute("versionDate", versionDate);
        System.getProperties().put(APPLICATION_VERSION, version);
        System.getProperties().put(APPLICATION_VERSION_YEAR, versionYear);
        System.getProperties().put(APPLICATION_VERSION_DATE,versionDate);
    }
}
