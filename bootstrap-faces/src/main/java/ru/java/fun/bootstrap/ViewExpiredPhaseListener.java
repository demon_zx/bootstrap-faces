package ru.java.fun.bootstrap;

import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;

/**
 * @author Terentjev Dmitry
 * Date: 24.12.13
 * Time: 12:34
 */
public class ViewExpiredPhaseListener implements PhaseListener {

    @Override
    public void afterPhase(PhaseEvent phaseEvent) {
        if (phaseEvent.getFacesContext().getExternalContext().getSession(false) != null) {
            phaseEvent.getFacesContext().getExternalContext().getSessionMap().put(ViewExpiredHandler.VIEW_EXPIRED_ATTRIBUTE, null);
        }
    }

    @Override
    public void beforePhase(PhaseEvent phaseEvent) {

    }

    @Override
    public PhaseId getPhaseId() {
        return PhaseId.RENDER_RESPONSE;
    }
}
