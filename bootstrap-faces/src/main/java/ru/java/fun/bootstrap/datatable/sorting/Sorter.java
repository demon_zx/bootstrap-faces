package ru.java.fun.bootstrap.datatable.sorting;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Terentjev Dmitry
 *         Date: 03.02.14
 *         Time: 13:28
 */
public class Sorter<T> implements Sortable<T> {

    private Map<String, String> orders = new HashMap<String, String>(), altOrders = new HashMap<String, String>(), mappingOrders = new HashMap<String, String>();
    private Map<String, SortableComparator<T>> fieldComparators = new HashMap<String, SortableComparator<T>>();
    private String defaultField;
    private boolean defaultAscending = true;
    private SortableComparator<T> currentComparator;

    public Sorter(List<SortableComparator<T>> comparatorList, Map<String, String> cssMappingOrders, String defaultField, boolean defaultDirect) {
        mappingOrders = cssMappingOrders;
        for (SortableComparator<T> tSortableComparator : comparatorList) {
            addComparator(tSortableComparator, defaultField, defaultDirect);
        }
        map();
    }


    private void addComparator(SortableComparator<T> comparator, String defaultField, boolean defaultDirect) {
        fieldComparators.put(comparator.getFieldName(), comparator);
        if (comparator.getFieldName().equals(defaultField)) {
            this.defaultField = defaultField;
            defaultAscending = defaultDirect;
            defaultSort();
        }
    }

    private void defaultSort() {
        if (defaultField != null) {
            SortableComparator<T> cmp = fieldComparators.get(defaultField);
            if (defaultAscending) {
                cmp.setState(SortableState.ascending);
            } else {
                cmp.setState(SortableState.descending);
            }
        }
    }

    @Override
    public void nextSort(String field) {
        SortableComparator<T> cmp = fieldComparators.get(field);
        cmp.nextState();
        for (SortableComparator<T> sc : fieldComparators.values()) {
            if (sc != cmp)
                sc.setState(SortableState.unsorted);
        }
        map();
    }

    @Override
    public List<T> sort(String field, List<T> data) {
        SortableComparator<T> cmp = fieldComparators.get(field) == null ? fieldComparators.get(defaultField) : fieldComparators.get(field);
        return sort(cmp, data);
    }

    private List<T> sort(SortableComparator<T> cmp, List<T> data) {
        currentComparator = cmp;
        return sort(data);
    }

    private void map() {
        for (String nm : fieldComparators.keySet()) {
            SortableComparator<T> c = fieldComparators.get(nm);
            String state = c.getState().toString();
            orders.put(nm, state);
            String nnn = mappingOrders.get(state);
            if (nnn == null)
                nnn = state;
            altOrders.put(nm, nnn);
        }
    }

    private List<T> sort(List<T> data) {
        if (currentComparator == null || data == null)
            return data;
        Collections.sort(data, currentComparator);
        return data;
    }

    @Override
    public Map<String, String> getOrders() {
        return orders;
    }

    @Override
    public Map<String, String> getAltOrders() {
        return altOrders;
    }


}
