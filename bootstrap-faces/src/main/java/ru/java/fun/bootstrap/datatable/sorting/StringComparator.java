package ru.java.fun.bootstrap.datatable.sorting;

import java.text.Collator;

/**
 * @author Terentjev Dmitry
 *         Date: 03.02.14
 *         Time: 13:18
 */
public class StringComparator<T> extends DefaultComparator<T> {

    private Get<T, String> get;
    private Collator c = Collator.getInstance();
    private String fieldName;

    public StringComparator(String fieldName, Get<T, String> get) {
        this.get = get;
        this.fieldName = fieldName;
    }

    @Override
    public int compare(T o1, T o2) {
        if (getState() == SortableState.unsorted)
            return 0;
        String v1 = get.value(o1);
        String v2 = get.value(o2);
        if (getState() == SortableState.ascending) {
            return c.compare(v1, v2);
        } else {
            return c.compare(v2, v1);
        }
    }

    @Override
    public String getFieldName() {
        return fieldName;
    }

}
