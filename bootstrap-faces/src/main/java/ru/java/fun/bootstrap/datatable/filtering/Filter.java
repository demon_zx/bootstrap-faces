package ru.java.fun.bootstrap.datatable.filtering;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Terentjev Dmitry
 *         Date: 29.05.2014
 *         Time: 16:08
 */
public class Filter<T> implements Filterable<T> {

    private String filter;
    private FilterChain<T> chain;

    public Filter(FilterChain<T> chain) {
        this.chain = chain;
    }

    @Override
    public List<T> filtering(List<T> listData) {
        if (listData == null || filter == null || filter.trim().isEmpty())
            return listData;
        List<T> r = new ArrayList<T>();
        String f = filter.trim().toLowerCase();
        for (T t : listData) {
            if (chain.isAccepted(t, f))
                r.add(t);
        }
        return r;
    }

    @Override
    public String getFilter() {
        return filter;
    }

    @Override
    public void setFilter(String filter) {
        this.filter = filter;
    }
}
