package ru.java.fun.bootstrap.datatable.sorting;

/**
 * @author Terentjev Dmitry
 *         Date: 03.02.14
 *         Time: 13:17
 */
public interface Get<T, E> {

    public E value(T entity);
}
