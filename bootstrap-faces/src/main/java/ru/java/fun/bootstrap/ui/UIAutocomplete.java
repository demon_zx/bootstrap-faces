package ru.java.fun.bootstrap.ui;

import ru.java.fun.bootstrap.util.RenderKitUtils;

import javax.faces.application.ResourceDependencies;
import javax.faces.application.ResourceDependency;
import javax.faces.component.FacesComponent;
import javax.faces.model.SelectItem;
import java.util.Iterator;

/**
 * @author Terentjev Dmitry
 *         Date: 11.12.13
 *         Time: 10:09
 */
@FacesComponent("autocomplete")
@ResourceDependencies({
        @ResourceDependency(name = "bootstrap/css/bootstrap.min.css", target = "head"),
        @ResourceDependency(name = "bootstrap/css/bootstrap-theme.min.css", target = "head"),
        @ResourceDependency(name = "bootstrap/css/typeahead.css",target = "head"),
        @ResourceDependency(library = "bootstrap/js", name = "jquery.min.js", target = "head"),
        @ResourceDependency(library = "bootstrap/js", name = "bootstrap.min.js", target = "head"),
        @ResourceDependency(library = "bootstrap/js", name = "typeahead.bundle.min.js", target = "head")
})
public class UIAutocomplete extends UIPanel {

    @Override
    public String getRendererType() {
        return "ru.java.fun.bootstrap.Autocomplete";
    }

    public String getTags() {
        StringBuilder sb = new StringBuilder(",");
        for (Iterator<SelectItem> it = RenderKitUtils.getSelectItems(getFacesContext(), this); it.hasNext(); ) {
            SelectItem si =  it.next();
            append(sb, (String) si.getValue());
        }
        return "[" + sb.substring(1) + "]";
    }

    private void append(StringBuilder sb, String value) {
//        sb.append("'").append(value).append("',");
        sb.append("{k:'").append(value).append("'},");
    }

    public String getFor(){
        return (String)getStateHelper().eval("for");
    }

    public void setFor(String id){
        getStateHelper().put("for",id);
    }

    public Integer getLimit(){
        return (Integer)getStateHelper().eval("limit",5);
    }

    public void setLimit(Integer limit){
        getStateHelper().put("limit",limit);
    }

    public Integer getMinLength(){
        return (Integer)getStateHelper().eval("minLength",1);
    }

    public void setMinLength(Integer minLength){
        getStateHelper().put("minLength",minLength);
    }
}
