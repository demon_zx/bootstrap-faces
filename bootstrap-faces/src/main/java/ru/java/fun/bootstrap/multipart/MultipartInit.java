package ru.java.fun.bootstrap.multipart;

import ru.java.fun.bootstrap.Bootstrap;

import javax.servlet.*;
import javax.servlet.annotation.WebListener;
import java.util.EnumSet;

/**
 * @author Terentjev Dmitry
 *         Date: 17.04.2014
 *         Time: 13:18
 */
@WebListener
public class MultipartInit implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        String useNative = sce.getServletContext().getInitParameter(Bootstrap.FILE_UPLOAD_USE_NATIVE_INPUT_FILE);
        useNative = useNative == null ? "false" : useNative;
        if (!Boolean.parseBoolean(useNative)) {
            ServletContext sc = sce.getServletContext();
            for (ServletRegistration sr : sc.getServletRegistrations().values()) {
                if ("javax.faces.webapp.FacesServlet".equals(sr.getClassName())) {
                    FilterRegistration.Dynamic filter = sc.addFilter("multipart", MultipartRequestFilter.class);
                    EnumSet<DispatcherType> dispatcherTypes = EnumSet.of(
                            DispatcherType.REQUEST, DispatcherType.FORWARD);
                    filter.addMappingForServletNames(dispatcherTypes, true, sr.getName());
                    break;
                }
            }
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}
