package ru.java.fun.bootstrap.datatable.filtering;

import ru.java.fun.bootstrap.datatable.sorting.Get;

/**
 * @author Terentjev Dmitry
 *         Date: 29.05.2014
 *         Time: 16:33
 */
public class IgnoreCaseFilterChain<T> implements FilterChain<T> {

    private FilterChain<T> nextFilter;
    private Get<T, String> value;

    public IgnoreCaseFilterChain(Get<T, String> value) {
        this.value = value;
    }

    @Override
    public FilterChain<T> addNextFilter(FilterChain<T> next) {
        nextFilter = next;
        return nextFilter;
    }

    @Override
    public boolean isAccepted(T entity, String filter) {
        String v = value.value(entity);
        return (v != null && v.toLowerCase().contains(filter))
                || nextFilter != null
                && nextFilter.isAccepted(entity, filter);
    }

}
