package ru.java.fun.bootstrap;

import javax.faces.application.ProjectStage;
import javax.faces.application.Resource;
import javax.faces.application.ResourceHandler;
import javax.faces.application.ResourceHandlerWrapper;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

/**
 * @author Terentjev Dmitry
 *         Date: 07.08.2014
 *         Time: 13:12
 */
public class BootstrapFacesResourceHandler extends ResourceHandlerWrapper {

    private ResourceHandler parent;

    public BootstrapFacesResourceHandler(ResourceHandler parent) {
        this.parent = parent;
    }

    @Override
    public ResourceHandler getWrapped() {
        return this.parent;
    }

    @Override
    public Resource createResource(String resourceName) {
//        System.out.println(resourceName);
        return super.createResource(unmin(resourceName, null));
    }

    @Override
    public Resource createResource(String resourceName, String libraryName) {
//        System.out.println(resourceName + " lib:" + libraryName);
        return super.createResource(unmin(resourceName, libraryName), libraryName);
    }

    @Override
    public Resource createResource(String resourceName, String libraryName, String contentType) {
//        System.out.println(resourceName + " lib:" + libraryName+" ct: "+contentType);
        return super.createResource(unmin(resourceName, libraryName), libraryName, contentType);
    }

    private String unmin(String name, String lib) {
        if (((lib == null && name.startsWith("bootstrap/") && (name.endsWith(".css"))) || ("bootstrap/js".equals(lib))) && FacesContext.getCurrentInstance().isProjectStage(ProjectStage.Development)) {
            return name.replace(".min", "");
        }
        return name;
    }

}
