package ru.java.fun.bootstrap.ui;

import javax.faces.application.ResourceDependencies;
import javax.faces.application.ResourceDependency;
import javax.faces.component.FacesComponent;
import javax.faces.component.UIOutput;

/**
 * @author Terentjev Dmitry
 *         Date: 11.12.13
 *         Time: 10:09
 */
@FacesComponent("expire")
@ResourceDependencies({
        @ResourceDependency(name = "bootstrap/css/bootstrap.min.css", target = "head"),
        @ResourceDependency(name = "bootstrap/css/bootstrap-theme.min.css", target = "head"),
        @ResourceDependency(library = "bootstrap/js", name = "jquery.min.js", target = "head"),
        @ResourceDependency(library = "javax.faces", name = "jsf.js"),
        @ResourceDependency(library = "bootstrap/js", name = "bootstrap.min.js", target = "head"),
        @ResourceDependency(library = "bootstrap/js", name = "jsf-bs.js", target = "head")
})
public class UIExpire extends UIOutput {

    @Override
    public String getRendererType() {
        return "ru.java.fun.bootstrap.Expire";
    }

    String title,message;

    public String getTitle() {
        return (String) getStateHelper().eval("title");
    }

    public void setTitle(String title) {
        getStateHelper().put("title",title);
    }

    public String getMessage() {
        return (String) getStateHelper().eval("message");
    }

    public void setMessage(String message) {
        getStateHelper().put("message",message);
    }
}
