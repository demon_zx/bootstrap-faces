package ru.java.fun.bootstrap.datatable.filtering;

import java.util.List;

/**
 * @author Terentjev Dmitry
 *         Date: 29.05.2014
 *         Time: 16:06
 */
public interface Filterable<T> {
    List<T> filtering(List<T> listData);

    String getFilter();

    void setFilter(String filter);
}
