package ru.java.fun.bootstrap.html;

import ru.java.fun.bootstrap.ui.UIMenu;

import javax.faces.component.UIComponent;
import javax.faces.component.UIPanel;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import javax.faces.render.FacesRenderer;
import javax.faces.render.Renderer;
import java.io.IOException;

/**
 * @author Terentjev Dmitry
 *         Date: 09.01.14
 *         Time: 9:55
 */
@FacesRenderer(rendererType = "ru.java.fun.bootstrap.Menu", componentFamily = UIPanel.COMPONENT_FAMILY)
public class HtmlMenu extends Renderer {

    @Override
    public void encodeBegin(FacesContext context, UIComponent _component) throws IOException {
        ResponseWriter w = context.getResponseWriter();
        UIMenu c = (UIMenu) _component;
        w.startElement("ul", c);
        w.writeAttribute("class", "nav navbar-nav " + (c.isToright() ? "navbar-right " : "") + c.getStyleClass(), "");
        if (c.getStyle() != null)
            w.writeAttribute("style", c.getStyle(), "");
        w.writeAttribute("id", c.getClientId(), "");
    }

    @Override
    public boolean getRendersChildren() {
        return true;
    }

    @Override
    public void encodeEnd(FacesContext context, UIComponent uiComponent) throws IOException {
        ResponseWriter w = context.getResponseWriter();
        w.endElement("ul");
    }
}
