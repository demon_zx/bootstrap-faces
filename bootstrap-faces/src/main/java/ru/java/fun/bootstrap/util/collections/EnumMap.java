package ru.java.fun.bootstrap.util.collections;

import java.util.HashMap;

/**
 * @author Terentjev Dmitry
 *         Date: 06.05.2014
 *         Time: 11:20
 */
public class EnumMap extends HashMap<Integer, String> {

    public EnumMap(Enum[] enumValues) {
        for (Enum t : enumValues) {
            put(t.ordinal(), t.toString());
        }
    }
}
