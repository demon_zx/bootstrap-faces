package ru.java.fun.bootstrap.html;

import ru.java.fun.bootstrap.ui.UIAttr;
import ru.java.fun.bootstrap.ui.UIData;

import javax.faces.component.UIComponent;
import javax.faces.component.UIOutput;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import javax.faces.render.FacesRenderer;
import javax.faces.render.Renderer;
import java.io.IOException;

/**
 * @author Terentjev Dmitry
 *         Date: 09.01.14
 *         Time: 9:55
 */
@FacesRenderer(rendererType = "ru.java.fun.bootstrap.Data", componentFamily = UIOutput.COMPONENT_FAMILY)
public class HtmlData extends Renderer {

    @Override
    public void encodeBegin(FacesContext context, UIComponent component) throws IOException {
        ResponseWriter w = context.getResponseWriter();
        UIData c = (UIData) component;
        w.writeAttribute("data-test","","");
    }
}
