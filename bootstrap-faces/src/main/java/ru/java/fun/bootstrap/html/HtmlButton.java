package ru.java.fun.bootstrap.html;

import ru.java.fun.bootstrap.ResponseWriterExtended;
import ru.java.fun.bootstrap.ui.UIButton;
import ru.java.fun.bootstrap.ui.UIMenuGroup;
import ru.java.fun.bootstrap.util.RenderKitUtils;

import javax.faces.component.UICommand;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.render.FacesRenderer;
import javax.faces.render.Renderer;
import java.io.IOException;

/**
 * @author Terentjev Dmitry
 *         Date: 09.01.14
 *         Time: 9:55
 */
@FacesRenderer(rendererType = "ru.java.fun.bootstrap.Button", componentFamily = UICommand.COMPONENT_FAMILY)
public class HtmlButton extends Renderer {

    @Override
    public void encodeBegin(FacesContext context, UIComponent _component) throws IOException {
        ResponseWriterExtended w = new ResponseWriterExtended(_component, context.getResponseWriter());
        UIButton c = (UIButton) _component;
        w.startElement("a");
        w.writeAttribute("href", "#");
        w.writeAttribute("id", c.getClientId());

        w.writeAttributeNN("style", c.getStyle());
//        w.writeAttributeNN("target", c.getTarget());
        w.writeAttributeNN("title", c.getTitle());
        String sc = c.getStyleClass();
        sc = sc == null ? "" : " " + sc;
        String size = c.getSize() != null ? " btn-" + c.getSize().toLowerCase() : "";
        boolean menu = c.getParent() instanceof UIMenuGroup;
        w.writeAttribute("class", (menu ? "dropdown-toggle " : "") + "btn btn-" + c.getContext() + size + sc);
        if (menu) {
            w.writeAttribute("data-toggle", "dropdown");
        }
        if (c.isDisabled()) {
            w.writeAttribute("disabled", "disabled");
        } else {
            if (!c.isNoAction())
                RenderKitUtils.renderHandler(context, c, null, "action", "onclick", c.getOnclick(), c.getTarget() != null ? c.getTarget() : "");
        }
    }

    @Override
    public boolean getRendersChildren() {
        return true;
    }

    @Override
    public void encodeEnd(FacesContext context, UIComponent uiComponent) throws IOException {
        ResponseWriterExtended w = new ResponseWriterExtended(uiComponent, context.getResponseWriter());
        UIButton c = (UIButton) uiComponent;
        w.endElement("a");
//        if (!c.getClientBehaviors().containsKey("action") && !c.isDisabled()) {
//            w.startElement("input");
//            w.writeAttribute("type", "hidden");
//            w.writeAttribute("name", c.getClientId());
//            w.writeAttribute("value", c.getClientId());
//            w.endElement("input");
//        }
    }

}
