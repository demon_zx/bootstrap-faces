package ru.java.fun.bootstrap;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import java.io.IOException;

/**
 * @author Terentjev Dmitry
 *         Date: 07.02.14
 *         Time: 11:02
 */
public class ResponseWriterExtended {

    private ResponseWriter w;
    private String instead = "";
    private UIComponent component;

    public ResponseWriterExtended(UIComponent cmp, ResponseWriter rw, String nullInstead) {
        w = rw;
        instead = nullInstead;
        component = cmp;
    }

    public ResponseWriterExtended(UIComponent cmp, ResponseWriter rw) {
        w = rw;
        component = cmp;
    }

    public ResponseWriterExtended(UIComponent cmp, FacesContext ctx) {
        this(cmp, ctx.getResponseWriter());
    }

    public ResponseWriterExtended id() throws IOException {
        return attr("id", component.getClientId());
    }

    public ResponseWriterExtended style(String style) throws IOException {
        return attrNN("style", style);
    }

    public ResponseWriterExtended styleClass(String styleClass) throws IOException {
        return attrNN("class", styleClass);
    }

    public ResponseWriterExtended styleClass(String prefixClasses, String styleClass) throws IOException {
        String v = styleClass != null ? " " + styleClass.trim() : "";
        return attrNN("class", prefixClasses + v);
    }

    public ResponseWriterExtended start(String tag) throws IOException {
        w.startElement(tag, component);
        return this;
    }

    public ResponseWriterExtended end(String tag) throws IOException {
        w.endElement(tag);
        return this;
    }

    /**
     * write if value not null
     */
    public ResponseWriterExtended attrNN(String attribute, Object value) throws IOException {
        writeAttributeNN(attribute, value);
        return this;
    }

    public ResponseWriterExtended attr(String attribute, Object value) throws IOException {
        writeAttribute(attribute, value);
        return this;
    }

    public ResponseWriterExtended text(Object text) throws IOException {
        writeText(text);
        return this;
    }

    public void startElement(String name) throws IOException {
        w.startElement(name, component);
    }

    public void endElement(String name) throws IOException {
        w.endElement(name);
    }

    public void writeAttribute(String name, Object value) throws IOException {
        Object _text = value;
        if (_text == null)
            _text = instead;
        w.writeAttribute(name, _text, "");
    }

    /**
     * write if value not null
     */
    public void writeAttributeNN(String name, Object value) throws IOException {
        if (value != null)
            w.writeAttribute(name, value, "");
    }

    public void writeText(Object text) throws IOException {
        Object _text = text;
        if (_text == null)
            _text = instead;
        w.writeText(_text, "");
    }
}
