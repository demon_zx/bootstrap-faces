package ru.java.fun.bootstrap.ui;

import javax.faces.application.ResourceDependencies;
import javax.faces.application.ResourceDependency;
import javax.faces.component.FacesComponent;
import javax.faces.component.UIPanel;

/**
 * @author Terentjev Dmitry
 * Date: 11.12.13
 * Time: 10:09
 */
@FacesComponent("buttonMenu")
@ResourceDependencies({
        @ResourceDependency(name = "bootstrap/css/bootstrap.min.css", target = "head"),
        @ResourceDependency(name = "bootstrap/css/bootstrap-theme.min.css", target = "head")
})
public class UIButtonMenu extends UIPanel {

    @Override
    public String getRendererType() {
        return "ru.java.fun.bootstrap.ButtonMenu";
    }

    public Integer getButtonLimit() {
        return (Integer) getStateHelper().eval("buttonLimit");
    }

    public void setButtonLimit(Integer buttonLimit) {
        getStateHelper().put("buttonLimit", buttonLimit);
    }


    public String getStyle() {
        return (String) getStateHelper().eval("style");
    }

    public void setStyle(String style) {
        getStateHelper().put("style", style);
    }

    public String getStyleClass() {
        return (String) getStateHelper().eval("styleClass", "");
    }

    public void setStyleClass(String styleClass) {
        getStateHelper().put("styleClass", styleClass);
    }

    public String getTitle() {
        return (String) getStateHelper().eval("title");
    }

    public void setTitle(String title) {
        getStateHelper().put("title", title);
    }

    public String getSize() {
        return (String) getStateHelper().eval("size");
    }

    public void setSize(String size) {
        getStateHelper().put("size", size);
    }

    public String getContext() {
        return (String) getStateHelper().eval("context", "default");
    }

    public void setContext(String context) {
        getStateHelper().put("context", context);
    }

    public String getAlign() {
        return (String) getStateHelper().eval("align", "left");
    }

    public void setAlign(String align) {
        getStateHelper().put("align", align);
    }
}
