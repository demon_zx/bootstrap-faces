package ru.java.fun.bootstrap.ui;

import javax.faces.application.ResourceDependencies;
import javax.faces.application.ResourceDependency;
import javax.faces.component.FacesComponent;
import javax.faces.component.behavior.ClientBehaviorHolder;
import javax.faces.context.FacesContext;

/**
 * @author Terentjev Dmitry
 *         Date: 07.02.14
 *         Time: 14:17
 */
@FacesComponent("button")
@ResourceDependencies({
        @ResourceDependency(name = "bootstrap/css/bootstrap.min.css", target = "head"),
        @ResourceDependency(name = "bootstrap/css/bootstrap-theme.min.css", target = "head"),
        @ResourceDependency(name = "bootstrap/css/bs-faces.css", target = "head"),
        @ResourceDependency(library = "bootstrap/js", name = "jquery.min.js", target = "head"),
        @ResourceDependency(library = "javax.faces", name = "jsf.js"),
        @ResourceDependency(library = "bootstrap/js", name = "bootstrap.min.js", target = "head"),
        @ResourceDependency(library = "bootstrap/js", name = "jsf-bs.js", target = "head")
})

public class UIButton extends UISimpleCommand implements ClientBehaviorHolder {
    @Override
    public String getRendererType() {
        return "ru.java.fun.bootstrap.Button";
    }

    public String getContext() {
        return (String) getStateHelper().eval("context", "default");
    }

    public void setContext(String context) {
        getStateHelper().put("context", context);
    }

    public String getSize() {
        return (String) getStateHelper().eval("size");
    }

    public void setSize(String size) {
        getStateHelper().put("size", size);
    }

    public String getTitle() {
        return (String) getStateHelper().eval("title");
    }

    public void setTitle(String title) {
        getStateHelper().put("title", title);
    }

    public boolean isNoAction() {
        return (Boolean) getStateHelper().eval("noAction", false);
    }

    public void setNoAction(boolean noAction) {
        getStateHelper().put("noAction", noAction);
    }

    public String getOnclick() {
        return (String) getStateHelper().eval("onclick");
    }

    public void setOnclick(String onclick) {
        getStateHelper().put("onclick", onclick);
    }

    public String getTarget() {
        return (String) getStateHelper().eval("target");
    }

    public void setTarget(String target) {
        getStateHelper().put("target", target);
    }

    @Override
    public Object saveState(FacesContext context) {
        return super.saveState(context);
    }

    @Override
    public void restoreState(FacesContext context, Object o) {
        super.restoreState(context, o);
    }

    public String getStyleClass() {
        return (String) getStateHelper().eval("styleClass");
    }

    public void setStyleClass(String styleClass) {
        getStateHelper().put("styleClass", styleClass);
    }

    public String getStyle() {
        return (String) getStateHelper().eval("style");
    }

    public void setStyle(String style) {
        getStateHelper().put("style", style);
    }

    public boolean isDisabled() {
        return (Boolean) getStateHelper().eval("disabled", false);
    }

    public void setDisabled(boolean disabled) {
        getStateHelper().put("disabled", disabled);
    }
}
