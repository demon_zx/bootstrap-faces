package ru.java.fun.bootstrap.html;

import ru.java.fun.bootstrap.ResponseWriterExtended;
import ru.java.fun.bootstrap.ui.UIMessages;

import javax.faces.application.FacesMessage;
import javax.faces.component.NamingContainer;
import javax.faces.component.UIComponent;
import javax.faces.component.UIOutput;
import javax.faces.context.FacesContext;
import javax.faces.render.FacesRenderer;
import javax.faces.render.Renderer;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

/**
 * @author Terentjev Dmitry
 *         Date: 09.01.14
 *         Time: 9:55
 */
@FacesRenderer(rendererType = "ru.java.fun.bootstrap.Messages", componentFamily = UIOutput.COMPONENT_FAMILY)
public class HtmlMessages extends Renderer {

    @Override
    public void encodeBegin(FacesContext context, UIComponent component) throws IOException {
        ResponseWriterExtended w = new ResponseWriterExtended(component, context);
        UIMessages c = (UIMessages) component;

        boolean summary = c.isShowSummary();
        boolean details = c.isGlobalOnly();
        String clientId = c.getFor();
        if (clientId == null) {
            if (c.isGlobalOnly()) {
                clientId = "";
            }
        }
        List<FacesMessage> messages = getMessages(context, clientId, c);
        String hide = messages.size() == 0 ? "display:none; " : c.getStyle();
        w.startElement("div");
        w.writeAttribute("id", c.getClientId());
        w.writeAttributeNN("style", hide);
        w.writeAttributeNN("class", c.getStyleClass());
        for (FacesMessage message : messages) {
            if (!message.isRendered()) {
                message.rendered();
                w.startElement("div");
                String type = "info";
                if (message.getSeverity() == FacesMessage.SEVERITY_ERROR) {
                    type = "danger";
                }
                if (message.getSeverity() == FacesMessage.SEVERITY_FATAL) {
                    type = "danger";
                }
                if (message.getSeverity() == FacesMessage.SEVERITY_WARN) {
                    type = "warning";
                }
                w.writeAttribute("class", "alert alert-" + type);
                w.writeAttribute("role", "alert");
                w.writeText(message.getSummary());
                //todo add write details
                w.endElement("div");
            }
        }
        w.endElement("div");
    }

    private List<FacesMessage> getMessages(FacesContext context, String forComponent, UIComponent component) {
        if (forComponent != null) {
            if (forComponent.isEmpty()) {
                return context.getMessageList(null);
            } else {
                UIComponent c = getForComponent(context, forComponent, component);
                if (c == null) {
                    return Collections.emptyList();
                }
                return context.getMessageList(c.getClientId(context));
            }
        } else {
            return context.getMessageList();
        }
    }

    protected UIComponent getForComponent(FacesContext context, String forComponent, UIComponent component) {
        UIComponent r = null;
        if (forComponent != null && !forComponent.isEmpty()) {
            r = findComponent(component, forComponent);
            if (r == null) {
                r = findComponentInTree(context.getViewRoot(), forComponent);
            }
        }
//        if (result == null) {
//            if (logger.isLoggable(Level.WARNING)) {
//                logger.warning(MessageUtils.getExceptionMessageString(
//                        MessageUtils.COMPONENT_NOT_FOUND_IN_VIEW_WARNING_ID,
//                        forComponent));
//            }
//        }
        return r;
    }

    private UIComponent findComponent(UIComponent component, String forComponent) {
        UIComponent c = component.findComponent(forComponent);
        if (c == null && component.getParent() != null) {
            c = findComponent(component.getParent(), forComponent);
        }
        return c;
    }

    private UIComponent findComponentInTree(UIComponent component, String forComponent) {
        UIComponent r = null;
        for (UIComponent c : component.getChildren()) {
            if (c instanceof NamingContainer) {
                try {
                    r = c.findComponent(forComponent);
                } catch (IllegalArgumentException iae) {
                    continue;
                }
                if (r == null) {
                    r = findComponentInTree(c, forComponent);
                }
                if (r != null) {
                    break;
                }
            }
        }
        return r;
    }

}
