package ru.java.fun.bootstrap.html;

import ru.java.fun.bootstrap.ResponseWriterExtended;
import ru.java.fun.bootstrap.ui.UIIcon;

import javax.faces.component.UIComponent;
import javax.faces.component.UIOutput;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import javax.faces.render.FacesRenderer;
import javax.faces.render.Renderer;
import java.io.IOException;

/**
 * @author Terentjev Dmitry
 *         Date: 09.01.14
 *         Time: 9:55
 */
@FacesRenderer(rendererType = "ru.java.fun.bootstrap.Icon", componentFamily = UIOutput.COMPONENT_FAMILY)
public class HtmlIcon extends Renderer {

    @Override
    public void encodeBegin(FacesContext context, UIComponent _component) throws IOException {
        ResponseWriterExtended w = new ResponseWriterExtended(_component, context);
        UIIcon c = (UIIcon) _component;
        w.startElement("span");
        w.writeAttribute("class", "glyphicon glyphicon-" + c.getName() + " " + c.getStyleClass());
        w.writeAttributeNN("style", c.getStyle());
        w.writeAttributeNN("title", c.getTitle());
        w.writeAttribute("id", c.getClientId());
    }

    @Override
    public boolean getRendersChildren() {
        return true;
    }

    @Override
    public void encodeEnd(FacesContext context, UIComponent uiComponent) throws IOException {
        ResponseWriter w = context.getResponseWriter();
        w.endElement("span");
    }
}
