package ru.java.fun.bootstrap.html;

import ru.java.fun.bootstrap.ResponseWriterExtended;
import ru.java.fun.bootstrap.ui.UIFormGroupColumn;

import javax.faces.component.UIPanel;
import javax.faces.context.FacesContext;
import javax.faces.render.FacesRenderer;
import java.io.IOException;

/**
 * @author Terentjev Dmitry
 *         Date: 09.01.14
 *         Time: 9:55
 */
@FacesRenderer(rendererType = "ru.java.fun.bootstrap.FormGroupColumn", componentFamily = UIPanel.COMPONENT_FAMILY)
public class HtmlFormGroupColumn extends RendererExtended<UIFormGroupColumn> {

    @Override
    public void encodeBegin(FacesContext context, UIFormGroupColumn component, ResponseWriterExtended w) throws IOException {
        w.start("div")
                .id()
                .styleClass(component.getStyleClass())
                .style(component.getStyle());
    }

    @Override
    public void encodeEnd(FacesContext context, UIFormGroupColumn uiComponent, ResponseWriterExtended w) throws IOException {
        w.end("div");
    }
}
