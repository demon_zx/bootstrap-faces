package ru.java.fun.bootstrap.behavior;

import javax.faces.component.UIComponent;
import javax.faces.component.behavior.ClientBehaviorHolder;
import javax.faces.context.FacesContext;
import javax.faces.view.BehaviorHolderAttachedObjectHandler;
import javax.faces.view.facelets.*;
import java.io.IOException;

/**
 * @author Terentjev Dmitry
 *         Date: 12.08.2014
 *         Time: 10:48
 */
//@FacesBehaviorRenderer(rendererType = "confirm")
public class ConfirmTagHandler extends TagHandler implements BehaviorHolderAttachedObjectHandler {


    private final TagAttribute message;
    private final TagAttribute title;
    private final TagAttribute style;
    private final TagAttribute styleClass;
    private final TagAttribute buttonYes;
    private final TagAttribute buttonNo;

    public ConfirmTagHandler(BehaviorConfig config) {
        super(config);
        message = this.getAttribute(ConfirmBehavior.MESSAGE);
        title = this.getAttribute(ConfirmBehavior.TITLE);
        style = this.getAttribute(ConfirmBehavior.STYLE);
        styleClass = this.getAttribute(ConfirmBehavior.STYLE_CLASS);
        buttonYes = this.getAttribute(ConfirmBehavior.BUTTON_YES);
        buttonNo = this.getAttribute(ConfirmBehavior.BUTTON_NO);
    }

    @Override
    public void apply(FaceletContext ctx, UIComponent parent) throws IOException {
        applyAttachedObject(ctx, parent, getEventName());
    }

    @Override
    public String getEventName() {
        return "action";
    }

    @Override
    public void applyAttachedObject(FacesContext context, UIComponent parent) {
        FaceletContext ctx = (FaceletContext) context.getAttributes().get(FaceletContext.FACELET_CONTEXT_KEY);
        applyAttachedObject(ctx, parent, getEventName());
    }

    public void applyAttachedObject(FaceletContext context, UIComponent parent, String eventName) {
        ConfirmBehavior b = new ConfirmBehavior();
//        b.set

        if (parent instanceof ClientBehaviorHolder) {
            setBehaviorAttribute(context, b, message, String.class);
            setBehaviorAttribute(context, b, title, String.class);
            setBehaviorAttribute(context, b, style, String.class);
            setBehaviorAttribute(context, b, styleClass, String.class);
            setBehaviorAttribute(context, b, buttonYes, String.class);
            setBehaviorAttribute(context, b, buttonNo, String.class);

            ClientBehaviorHolder h = (ClientBehaviorHolder) parent;
            String name =eventName == null ? h.getDefaultEventName() : eventName;
            h.addClientBehavior(name, b);
        } else {
            throw new TagException(tag, String.format("Unable to attach <b:%s> to non-ClientBehaviorHolder parent", tag.getLocalName()));
        }
    }

    private void setBehaviorAttribute(FaceletContext ctx, ConfirmBehavior behavior, TagAttribute attr, Class type) {
        if (attr != null) {
            behavior.setValueExpression(attr.getLocalName(), attr.getValueExpression(ctx, type));
        }
    }

    @Override
    public String getFor() {
        return null;
    }
}
