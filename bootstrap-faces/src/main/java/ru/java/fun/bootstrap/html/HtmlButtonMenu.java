package ru.java.fun.bootstrap.html;

import ru.java.fun.bootstrap.ResponseWriterExtended;
import ru.java.fun.bootstrap.ui.UIButtonMenu;

import javax.faces.component.UICommand;
import javax.faces.component.UIComponent;
import javax.faces.component.UIPanel;
import javax.faces.context.FacesContext;
import javax.faces.render.FacesRenderer;
import javax.faces.render.Renderer;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Terentjev Dmitry
 * Date: 09.01.14
 * Time: 9:55
 */
@FacesRenderer(rendererType = "ru.java.fun.bootstrap.ButtonMenu", componentFamily = UIPanel.COMPONENT_FAMILY)
public class HtmlButtonMenu extends Renderer {

    private List<UIComponent> getButtons(UIComponent component) {
        List<UIComponent> buttons = new ArrayList<UIComponent>();
        for (UIComponent uic : component.getChildren()) {
            if (uic instanceof UICommand && uic.isRendered()) {
                buttons.add(uic);
            }
        }
        return buttons;
    }

    @Override
    public void encodeBegin(FacesContext context, UIComponent component) throws IOException {
        ResponseWriterExtended w = new ResponseWriterExtended(component, context.getResponseWriter());
        UIButtonMenu c = (UIButtonMenu) component;
        List<UIComponent> buttons = getButtons(component);
        w.startElement("div");
        w.writeAttribute("id", c.getClientId());
        w.writeAttributeNN("class", c.getStyleClass());
        w.writeAttributeNN("style", c.getStyle());
        if (buttons.size() > c.getButtonLimit()) {
            w.startElement("div");
            w.writeAttribute("class", "btn-group");
            UIComponent button = c.getFacet("button");
            if (button == null) {
                String size = c.getSize() != null ? " btn-" + c.getSize().toLowerCase() : "";
                w.startElement("button");
                w.writeAttribute("class", "btn btn-default dropdown-toggle btn-" + c.getContext() + size);
                w.writeAttribute("data-toggle", "dropdown");
                if (c.getTitle() != null)
                    w.writeText(c.getTitle());
                w.startElement("span");
                w.writeAttribute("class", "glyphicon glyphicon-chevron-down");
                w.endElement("span");
                w.endElement("button");
            } else {
                button.encodeAll(context);
            }

            w.startElement("ul");
            w.writeAttribute("class", "dropdown-menu" + ("right".equalsIgnoreCase(c.getAlign()) ? " dropdown-menu-right" : ""));
            w.writeAttribute("role", "menu");
            int cnt = buttons.size() - c.getButtonLimit() + 1;
            for (int i = 0; i < cnt; i++) {
                w.startElement("li");
                buttons.get(i).encodeAll(context);
                w.endElement("li");
            }
            w.endElement("ul");
            w.endElement("div");
        }
    }

    @Override
    public boolean getRendersChildren() {
        return true;
    }

    @Override
    public void encodeChildren(FacesContext context, UIComponent component) throws IOException {
        UIButtonMenu c = (UIButtonMenu) component;
        List<UIComponent> buttons = getButtons(component);
        if (buttons.size() <= c.getButtonLimit()) {
            super.encodeChildren(context, component);
        } else {
            int skip = buttons.size() - c.getButtonLimit() + 1;
            for (int i = skip; i < buttons.size(); i++) {
                buttons.get(i).encodeAll(context);
            }
        }
    }

    @Override
    public void encodeEnd(FacesContext context, UIComponent component) throws IOException {
        ResponseWriterExtended w = new ResponseWriterExtended(component, context.getResponseWriter());
        w.endElement("div");
    }
}
