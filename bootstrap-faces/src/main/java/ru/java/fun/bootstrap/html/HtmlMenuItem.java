package ru.java.fun.bootstrap.html;

import ru.java.fun.bootstrap.ResponseWriterExtended;
import ru.java.fun.bootstrap.ui.UIMenuItem;

import javax.faces.component.UIComponent;
import javax.faces.component.UIOutput;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import javax.faces.render.FacesRenderer;
import javax.faces.render.Renderer;
import java.io.IOException;

/**
 * @author Terentjev Dmitry
 *         Date: 09.01.14
 *         Time: 9:55
 */
@FacesRenderer(rendererType = "ru.java.fun.bootstrap.MenuItem", componentFamily = UIOutput.COMPONENT_FAMILY)
public class HtmlMenuItem extends Renderer {

    @Override
    public void encodeBegin(FacesContext context, UIComponent _component) throws IOException {
        ResponseWriterExtended w = new ResponseWriterExtended(_component,context);
        UIMenuItem c = (UIMenuItem) _component;
        w.startElement("li");
        w.writeAttribute("id",c.getClientId());
        w.writeAttributeNN("class", c.getStyleClass());
        w.writeAttributeNN("style", c.getStyle());
    }

    @Override
    public boolean getRendersChildren() {
        return true;
    }

    @Override
    public void encodeEnd(FacesContext context, UIComponent uiComponent) throws IOException {
        super.encodeEnd(context, uiComponent);
        ResponseWriter w = context.getResponseWriter();
        w.endElement("li");
    }
}
