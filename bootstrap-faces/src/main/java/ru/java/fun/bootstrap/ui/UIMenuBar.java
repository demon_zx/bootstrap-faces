package ru.java.fun.bootstrap.ui;

import javax.faces.application.ResourceDependencies;
import javax.faces.application.ResourceDependency;
import javax.faces.component.FacesComponent;
import javax.faces.component.UIPanel;

/**
 * @author Terentjev Dmitry
 *         Date: 11.12.13
 *         Time: 10:09
 */
@FacesComponent("menuBar")
@ResourceDependencies({
        @ResourceDependency(name = "bootstrap/css/bootstrap.min.css", target = "head"),
        @ResourceDependency(name = "bootstrap/css/bootstrap-theme.min.css", target = "head"),
        @ResourceDependency(library = "bootstrap/js", name = "jquery.min.js", target = "head"),
        @ResourceDependency(library = "bootstrap/js", name = "bootstrap.min.js", target = "head"),
        @ResourceDependency(library = "bootstrap/js", name = "jsf-bs.js", target = "head")
})
public class UIMenuBar extends UIPanel {

    @Override
    public String getRendererType() {
        return "ru.java.fun.bootstrap.MenuBar";
    }

    public boolean isFixed() {
        return (Boolean) getStateHelper().eval("fixed",false);
    }

    public void setFixed(boolean fixed) {
        getStateHelper().put("fixed",fixed);
    }

    public String getBrand() {
        return (String) getStateHelper().eval("brand");
    }

    public void setBrand(String brand) {
        getStateHelper().put("brand",brand);
    }

    public boolean isInverse() {
        return (Boolean) getStateHelper().eval("inverse",false);
    }

    public void setInverse(boolean inverse) {
        getStateHelper().put("inverse",inverse);
    }

    public String getStyle() {
        return (String) getStateHelper().eval("style");
    }

    public void setStyle(String style) {
        getStateHelper().put("style",style);
    }

    public String getStyleClass() {
        return (String) getStateHelper().eval("styleClass","");
    }

    public void setStyleClass(String styleClass) {
        getStateHelper().put("styleClass",styleClass);
    }

    public String getBrandTitle() {
        return (String) getStateHelper().eval("brandTitle");
    }

    public void setBrandTitle(String brandTitle) {
        getStateHelper().put("brandTitle",brandTitle);
    }

    public int getContentPadding() {
        return (Integer) getStateHelper().eval("contentPadding",10);
    }

    public void setContentPadding(int contentPadding) {
        getStateHelper().put("contentPadding",contentPadding);
    }

    public boolean isTop() {
        return (Boolean) getStateHelper().eval("top",true);
    }

    public void setTop(boolean top) {
        getStateHelper().put("top",top);
    }
}
