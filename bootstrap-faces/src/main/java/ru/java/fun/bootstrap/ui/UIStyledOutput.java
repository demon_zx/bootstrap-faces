package ru.java.fun.bootstrap.ui;

import javax.faces.component.UIOutput;

/**
 * @author Terentjev Dmitry
 *         Date: 14.05.2014
 *         Time: 16:40
 */
public class UIStyledOutput extends UIOutput {

    public String getStyle() {
        return (String) getStateHelper().eval("style");
    }

    public void setStyle(String style) {
        getStateHelper().put("style", style);
    }

    public String getStyleClass() {
        return (String) getStateHelper().eval("styleClass", "");
    }

    public void setStyleClass(String styleClass) {
        getStateHelper().put("styleClass", styleClass);
    }
}