package ru.java.fun.bootstrap.html;

import ru.java.fun.bootstrap.ResponseWriterExtended;
import ru.java.fun.bootstrap.ui.UITimer;
import ru.java.fun.bootstrap.util.RenderKitUtils;

import javax.faces.component.UICommand;
import javax.faces.component.UIComponent;
import javax.faces.component.behavior.ClientBehavior;
import javax.faces.component.behavior.ClientBehaviorContext;
import javax.faces.context.FacesContext;
import javax.faces.render.FacesRenderer;
import javax.faces.render.Renderer;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author Terentjev Dmitry
 *         Date: 09.01.14
 *         Time: 9:55
 */
@FacesRenderer(rendererType = "ru.java.fun.bootstrap.Timer", componentFamily = UICommand.COMPONENT_FAMILY)
public class HtmlTimer extends Renderer {

    @Override
    public void encodeBegin(FacesContext context, UIComponent _component) throws IOException {
        ResponseWriterExtended w = new ResponseWriterExtended(_component, context.getResponseWriter());
        UITimer c = (UITimer) _component;
        w.startElement("input");
        w.writeAttribute("type", "submit");
        w.writeAttribute("id", c.getClientId());
        w.writeAttribute("style", "display:none");
        if (c.isDisabled()) {
            w.writeAttribute("disabled", "disabled");
        } else {
            Map<String, List<ClientBehavior>> map = c.getClientBehaviors();
            String click;
            if (map.containsKey("action")) {
                ClientBehaviorContext ctx = ClientBehaviorContext.createClientBehaviorContext(context,
                        c, "action", c.getClientId(), Collections.<ClientBehaviorContext.Parameter>emptyList());
                click = map.get("action").get(0).getScript(ctx) + ";return false;";
            } else {
                click = "jsf.bootstrap.submit('" + RenderKitUtils.getFormClientId(c, context) + "','" + c.getClientId() + "');";
            }
            w.writeAttribute("onclick", click);
        }
    }

    @Override
    public boolean getRendersChildren() {
        return false;
    }

    @Override
    public void encodeEnd(FacesContext context, UIComponent uiComponent) throws IOException {
        ResponseWriterExtended w = new ResponseWriterExtended(uiComponent, context.getResponseWriter());
        UITimer c = (UITimer) uiComponent;
        w.endElement("input");
        if (!c.isDisabled()) {
            w.startElement("script");
            w.writeAttribute("type", "text/javascript");
            String id = c.getClientId().replaceAll(":", "\\\\\\\\:");
            w.writeText("$(function () {\n" +
                    "        var f = function () {\n" +
                    " if(" + c.getJsEnable() + "){\n" +
                    "            $('#" + id + "').click();}\n" +
                    "            setTimeout(f," + c.getInterval() + ");\n" +
                    "        };\n" +
                    "        setTimeout(f," + c.getInterval() + ");\n" +
                    "    }\n" +
                    ");");
            w.endElement("script");
        }
    }

}
