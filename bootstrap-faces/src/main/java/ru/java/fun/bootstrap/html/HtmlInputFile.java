package ru.java.fun.bootstrap.html;

import ru.java.fun.bootstrap.ResponseWriterExtended;
import ru.java.fun.bootstrap.ui.UIInputFile;

import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import javax.faces.render.FacesRenderer;
import javax.faces.render.Renderer;
import java.io.IOException;

/**
 * @author Terentjev Dmitry
 *         Date: 09.01.14
 *         Time: 9:55
 */
@FacesRenderer(rendererType = "ru.java.fun.bootstrap.InputFile", componentFamily = UIInput.COMPONENT_FAMILY)
public class HtmlInputFile extends Renderer {

    @Override
    public void encodeBegin(FacesContext context, UIComponent _component) throws IOException {
        ResponseWriterExtended w = new ResponseWriterExtended(_component, context);
        UIInputFile c = (UIInputFile) _component;
        w.startElement("input");
        w.writeAttribute("type", "file");
        w.writeAttributeNN("class", c.getStyle());
        w.writeAttribute("id", c.getClientId());
        if (c.isMultiple())
            w.writeAttribute("multiple", "multiple");
        w.writeAttribute("name", c.getClientId() + (c.isMultiple() ? "[]" : ""));
        w.writeAttributeNN("class", c.getStyleClass());
        w.writeAttributeNN("accept", c.getAccept());
        if (c.isDisabled())
            w.writeAttribute("disabled", "disabled");
    }

    @Override
    public boolean getRendersChildren() {
        return true;
    }

    @Override
    public void encodeEnd(FacesContext context, UIComponent uiComponent) throws IOException {
        ResponseWriter w = context.getResponseWriter();
        w.endElement("input");
    }

}
