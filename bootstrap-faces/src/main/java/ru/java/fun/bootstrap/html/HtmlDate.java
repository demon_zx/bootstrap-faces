package ru.java.fun.bootstrap.html;

import ru.java.fun.bootstrap.ui.UIDate;
import ru.java.fun.bootstrap.util.RenderKitUtils;

import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import javax.faces.render.FacesRenderer;
import javax.faces.render.Renderer;
import java.io.IOException;

/**
 * @author Terentjev Dmitry
 *         Date: 09.01.14
 *         Time: 9:55
 */
@FacesRenderer(rendererType = "ru.java.fun.bootstrap.Date", componentFamily = UIInput.COMPONENT_FAMILY)
public class HtmlDate extends Renderer {

    @Override
    public void encodeBegin(FacesContext context, UIComponent _component) throws IOException {
        ResponseWriter w = context.getResponseWriter();
        UIDate c = (UIDate) _component;
        if (c.getConverter() == null) {
            c.initConverter();
        }
        w.startElement("div", c);
        w.writeAttribute("class", "input-group date", "");
        w.writeAttribute("id", c.getClientId(), "");
        w.startElement("input", c);
        w.writeAttribute("class", "form-control " + c.getStyleClass(), "");
        if (c.getStyle() != null)
            w.writeAttribute("style", c.getStyle(), "");
        if (c.isDisabled())
            w.writeAttribute("disabled", "disabled", "");
        w.writeAttribute("id", c.getClientId() + ":date", "");
        w.writeAttribute("name", c.getClientId() + ":date", "");
        w.writeAttribute("value", c.getConverter().getAsString(context, c, c.getValue()), "");
    }

    @Override
    public boolean getRendersChildren() {
        return true;
    }

    @Override
    public void encodeEnd(FacesContext context, UIComponent uiComponent) throws IOException {
        ResponseWriter w = context.getResponseWriter();
        w.endElement("input");
        w.startElement("span", uiComponent);
        w.writeAttribute("class", "input-group-addon", "");
        w.startElement("span", uiComponent);
        w.writeAttribute("class", "glyphicon glyphicon-calendar", "");
        w.endElement("span");
        w.endElement("span");
        w.endElement("div");
        UIDate c = (UIDate) uiComponent;
        w.startElement("script", c);
        w.writeAttribute("type", "text/javascript", "");
        String _id = "#" + c.getClientId().replaceAll(":", "\\\\\\\\:") + "";
        String id = "'" + _id + "'";
        String jf = javascriptFormat(c.getPattern());
        String dis;
        if (c.isDisabled()) {
            dis = "$('" + _id + ">span').css('cursor','not-allowed').unbind('click');";
        } else {
            dis = "$('" + _id + ">span').css('cursor','pointer');";
        }
        String change = RenderKitUtils.scriptHandler(context, c, null, "change", c.getOnchange(), "");
        String show = RenderKitUtils.scriptHandler(context, c, null, "show", c.getOnshow(), "");
        String hide = RenderKitUtils.scriptHandler(context, c, null, "hide", c.getOnhide(), "");
        StringBuilder events = new StringBuilder();
        chain(change, "changeDate", events);
        chain(show, "show", events);
        chain(hide, "hide", events);
        w.writeText("$(function(){\n$(" + id + ").datepicker({format: '" + jf + "',todayBtn:'linked',language:'ru',autoclose: true})" + events.toString() + ";" + dis + "}\n)", "");
        w.endElement("script");
    }

    private void chain(String script, String name, StringBuilder b) {
        if (!script.isEmpty()) {
            b.append(".on('").append(name).append("',function(event){").append(script).append("})");
        }
    }

    private String javascriptFormat(String f) {
        String s = f;
        if (s.contains("MMMM")) {
            s = s.replaceAll("MMMM", "MM");
        } else {
            if (s.contains("MMM")) {
                s = s.replaceAll("MMM", "M");
            } else {
                if (s.contains("MM")) {
                    s = s.replaceAll("MM", "mm");
                } else {
                    if (s.contains("M"))
                        s = s.replaceAll("M", "m");
                }
            }
        }
        s = s.replaceAll("D", "o");
        if (s.contains("EEEE")) {
            s = s.replaceAll("EEEE", "DD");
        } else {
            if (s.contains("EE")) {
                s = s.replaceAll("E{2,3}", "DD");
            }
        }
        return s;
    }

}
