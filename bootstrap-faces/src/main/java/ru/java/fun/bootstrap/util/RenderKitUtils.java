package ru.java.fun.bootstrap.util;

import ru.java.fun.bootstrap.ResponseWriterExtended;
import ru.java.fun.bootstrap.ui.UIModal;

import javax.faces.component.UICommand;
import javax.faces.component.UIComponent;
import javax.faces.component.UIForm;
import javax.faces.component.behavior.ClientBehavior;
import javax.faces.component.behavior.ClientBehaviorContext;
import javax.faces.component.behavior.ClientBehaviorHint;
import javax.faces.component.behavior.ClientBehaviorHolder;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import java.io.IOException;
import java.util.*;

/**
 * @author Terentjev Dmitry
 *         Date: 31.03.2014
 *         Time: 15:35
 */
public class RenderKitUtils {

    public static boolean isClicked(FacesContext context, String clientId) {
        return context.getExternalContext().getRequestParameterMap().containsKey(clientId) || isPartialOrBehaviorAction(context, clientId);
    }

    // Check the request parameters to see whether an action event has
    // been triggered either via jsf.ajax.request() or via a submitting
    // behavior.
    public static boolean isPartialOrBehaviorAction(FacesContext context,
                                                    String clientId) {
        if ((clientId == null) || (clientId.length() == 0)) {
            return false;
        }

        ExternalContext external = context.getExternalContext();
        Map<String, String> params = external.getRequestParameterMap();

        String source = params.get("javax.faces.source");
        if (!clientId.equals(source)) {
            return false;
        }

        // First check for a Behavior action event.
        String behaviorEvent = params.get("javax.faces.behavior.event");
        if (null != behaviorEvent) {
            return ("action".equals(behaviorEvent));
        }

        // Not a Behavior-related request.  Check for jsf.ajax.request()
        // request params.
        String partialEvent = params.get("javax.faces.partial.event");

        return ("click".equals(partialEvent));
    }

    /**
     * <p>Utility method to return the client ID of the parent form.</p>
     *
     * @param component typically a command component
     * @param context   the <code>FacesContext</code> for the current request
     * @return the client ID of the parent form, if any
     */
    public static String getFormClientId(UIComponent component,
                                         FacesContext context) {

        UIComponent parent = component.getParent();
        while (parent != null) {
            if (parent instanceof UIForm) {
                break;
            }
            parent = parent.getParent();
        }

        UIForm form = (UIForm) parent;
        if (form != null) {
            return form.getClientId(context);
        }

        return null;
    }

    private static List<ClientBehavior> getClientBehaviors(UIComponent component, String event) {
        if (component instanceof ClientBehaviorHolder) {
            ClientBehaviorHolder bHolder = (ClientBehaviorHolder) component;
            Map<String, List<ClientBehavior>> behaviors = bHolder.getClientBehaviors();
            if (null != behaviors && behaviors.containsKey(event)) {
                return behaviors.get(event);
            }
        }
        return Collections.emptyList();
    }

    public static boolean isDisabled(UIComponent c) {
        return (Boolean.valueOf(String.valueOf(c.getAttributes().get("disabled"))));
    }

    public static void renderHandler(FacesContext context,
                                     UIComponent component,
                                     Collection<ClientBehaviorContext.Parameter> params,
                                     String event, String handlerName, String userHandler, String target)
            throws IOException {
        if (isDisabled(component))
            return;
        ResponseWriterExtended w = new ResponseWriterExtended(component, context);
        String handler = scriptHandler(context, component, params, event, userHandler, target);
        w.writeAttribute(handlerName, handler);
    }

    public static String scriptHandler(FacesContext context,
                                       UIComponent component,
                                       Collection<ClientBehaviorContext.Parameter> params,
                                       String event, String userHandler, String target)
            throws IOException {
        if (isDisabled(component))
            return "";
        List<ClientBehavior> behaviors = getClientBehaviors(component, event);
        if (params == null) {
            params = Collections.emptyList();
        }
        String handler = "";
        boolean action = false;
        if (component instanceof UICommand) {
            action = ((UICommand) component).getActionExpression() != null;
        }
        switch (getHandlerType(behaviors, userHandler, action, params)) {

            case USER_HANDLER_ONLY:
                handler = userHandler;
                break;

            case SINGLE_BEHAVIOR_ONLY:
                assert behaviors != null;
                handler = getBehaviorScript(context, component, event, behaviors.get(0), params) + ";return false;";
                break;

            case SUBMIT_ONLY:
                handler = getSubmitScript(context, component, target);
                break;

            case CHAIN:
//                StringBuilder sb = new StringBuilder("jsf.util.chain(this,event");
                //firefox event undefined fix
                StringBuilder sb = new StringBuilder("jsf.util.chain(this,");
                if (component instanceof UIModal) {
                    sb.append("null");
                } else {
                    sb.append("event");
                }

                assert behaviors != null;
                boolean existSubmit = false;
                if (userHandler != null) {
                    String t = scriptEscape(userHandler);
                    sb.append(",'").append(t).append("'");
                }
                for (ClientBehavior b : behaviors) {
                    existSubmit |= isSubmitting(b);
                    String t = scriptEscape(getBehaviorScript(context, component, event, b, params));
                    sb.append(",'").append(t).append("'");
                }
                if (!existSubmit) {
                    String t = scriptEscape(getSubmitScript(context, component, ""));
                    sb.append(",'").append(t).append("'");
                }
                sb.append(");return false;");
                handler = sb.toString();
                break;
        }
        return handler;
    }

    private static String scriptEscape(String script) {
        return script.replaceAll("\\\\", "\\\\\\\\").replaceAll("'", "\\\\'");
    }

    private static String getSubmitScript(FacesContext context, UIComponent component, String target) {
        return "jsf.bootstrap.submit('" + getFormClientId(component, context) + "','" + component.getClientId() + "','" + target + "');";
    }

    private static String getBehaviorScript(FacesContext context, UIComponent component, String event,
                                            ClientBehavior behavior,
                                            Collection<ClientBehaviorContext.Parameter> params) {
        if (behavior == null)
            return null;
        ClientBehaviorContext bCtx = ClientBehaviorContext.createClientBehaviorContext(
                context,
                component,
                event,
                component.getClientId(),
                params);
        return behavior.getScript(bCtx);
    }

    private static HandlerType getHandlerType(List<ClientBehavior> behaviors, String userHandler, boolean action,
                                              Collection<ClientBehaviorContext.Parameter> params) {
        if (behaviors.isEmpty()) {
            if (userHandler != null && params.isEmpty() && !action)
                return HandlerType.USER_HANDLER_ONLY;
            return userHandler == null ? (action ? HandlerType.SUBMIT_ONLY : HandlerType.NONE) : HandlerType.CHAIN;
        }

        if (behaviors.size() == 1 && userHandler == null) {
            ClientBehavior behavior = behaviors.get(0);

            if (isSubmitting(behavior) || params.isEmpty())
                return HandlerType.SINGLE_BEHAVIOR_ONLY;
        }

        return HandlerType.CHAIN;
    }

    private static boolean isSubmitting(ClientBehavior b) {
        return b.getHints().contains(ClientBehaviorHint.SUBMITTING);
    }

    public static Iterator<SelectItem> getSelectItems(FacesContext context, UIComponent component) {
        return new SelectItemIterator<SelectItem>(context, component);
    }

    private static enum HandlerType {

        // Indicates that we only have a user handler - nothing else
        USER_HANDLER_ONLY,

        // Indicates that we only have a single behavior - no chaining
        SINGLE_BEHAVIOR_ONLY,

        // Indicates that we only render the mojarra.jsfcljs() script
        SUBMIT_ONLY,

        // Indicates that we've got a chain
        CHAIN,
        //no handler
        NONE
    }
}
