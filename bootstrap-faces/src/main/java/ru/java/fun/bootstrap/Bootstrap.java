package ru.java.fun.bootstrap;

import ru.java.fun.bootstrap.util.FacesUtils;

import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.servlet.ServletContext;

/**
 * @author Terentjev Dmitry
 *         Date: 12.12.13
 *         Time: 16:36
 */
@ManagedBean(eager = true)
@ApplicationScoped
public class Bootstrap {
    public static final String FILE_UPLOAD_SIZE_THRESHOLD = "ru.java.fun.bootstrap.FILE_UPLOAD_SIZE_THRESHOLD";
    public static final String FILE_UPLOAD_USE_NATIVE_INPUT_FILE = "ru.java.fun.bootstrap.FILE_UPLOAD_USE_NATIVE_INPUT_FILE";
    private String theme = "default";
    private boolean packedResources = false;
    private int maxFileSize = 0;

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public boolean isPackedResources() {
        return packedResources;
    }

    public void setPackedResources(boolean packedResources) {
        this.packedResources = packedResources;
    }

    @PostConstruct
    public void onCreate() {
        ServletContext sc = (ServletContext) FacesUtils.getContext().getContext();
        maxFileSize = getInitParameter(sc, FILE_UPLOAD_SIZE_THRESHOLD, 1024 * 1024 * 10);
        sc.setAttribute(FILE_UPLOAD_SIZE_THRESHOLD, maxFileSize);
//        FacesContext.getCurrentInstance().getApplication().addBehavior(AjaxBehavior.BEHAVIOR_ID, TestBehavior.class.getName());
    }

    public int getMaxFileSize() {
        return maxFileSize;
    }

    private String getInitParameter(ServletContext sc, String name, String defaultValue) {
        String v = sc.getInitParameter(name);
        return v == null ? defaultValue : v;
    }

    private int getInitParameter(ServletContext sc, String name, int defaultValue) {
        return Integer.parseInt(getInitParameter(sc, name, String.valueOf(defaultValue)));
    }
}
