package ru.java.fun.bootstrap.html;

import ru.java.fun.bootstrap.ResponseWriterExtended;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.render.Renderer;
import java.io.IOException;

/**
 * @author Terentjev Dmitry
 *         Date: 28.01.2015
 *         Time: 9:41
 */
public abstract class RendererExtended<T extends UIComponent> extends Renderer {

    @Override
    public void encodeBegin(FacesContext context, UIComponent component) throws IOException {
        encodeBegin(context, (T) component, new ResponseWriterExtended(component, context));
    }

    public void encodeBegin(FacesContext context, T component, ResponseWriterExtended writer) throws IOException {

    }

    @Override
    public void encodeEnd(FacesContext context, UIComponent component) throws IOException {
        encodeEnd(context, (T) component, new ResponseWriterExtended(component, context));
    }

    public void encodeEnd(FacesContext context, T component, ResponseWriterExtended writer) throws IOException {
    }

    @Override
    public void encodeChildren(FacesContext context, UIComponent component) throws IOException {
        encodeChildren(context, (T) component, new ResponseWriterExtended(component, context));
    }

    public void encodeChildren(FacesContext context, T component, ResponseWriterExtended writer) throws IOException {
    }
}
