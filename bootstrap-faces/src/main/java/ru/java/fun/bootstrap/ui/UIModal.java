package ru.java.fun.bootstrap.ui;

import javax.faces.application.ResourceDependencies;
import javax.faces.application.ResourceDependency;
import javax.faces.component.FacesComponent;
import javax.faces.component.behavior.ClientBehavior;
import javax.faces.component.behavior.ClientBehaviorHolder;
import javax.faces.context.FacesContext;
import java.util.*;

/**
 * @author Terentjev Dmitry
 *         Date: 11.12.13
 *         Time: 10:09
 */
@FacesComponent("modal")
@ResourceDependencies({
        @ResourceDependency(name = "bootstrap/css/bootstrap.min.css", target = "head"),
        @ResourceDependency(name = "bootstrap/css/bootstrap-theme.min.css", target = "head"),
        @ResourceDependency(library = "bootstrap/js", name = "jquery.min.js", target = "head"),
        @ResourceDependency(library = "javax.faces", name = "jsf.js"),
        @ResourceDependency(library = "bootstrap/js", name = "bootstrap.min.js", target = "head"),
        @ResourceDependency(library = "bootstrap/js", name = "jsf-bs.js", target = "head")
})
public class UIModal extends UIPanel implements ClientBehaviorHolder {

    protected static final String DEFAULT_EVENT = "show";
    protected static final Collection<String> EVENT_NAMES = Collections.unmodifiableCollection(Arrays.asList(DEFAULT_EVENT, "hide"));

    @Override
    public String getRendererType() {
        return "ru.java.fun.bootstrap.Modal";
    }

    @Override
    public Collection<String> getEventNames() {
        return EVENT_NAMES;
    }

    @Override
    public String getDefaultEventName() {
        return DEFAULT_EVENT;
    }

    public String getStyle() {
        return (String) getStateHelper().eval("style");
    }

    public void setStyle(String style) {
        getStateHelper().put("style", style);
    }

    public String getStyleClass() {
        return (String) getStateHelper().eval("styleClass", "");
    }

    public void setStyleClass(String styleClass) {
        getStateHelper().put("styleClass", styleClass);
    }

    public String getTitle() {
        return (String) getStateHelper().eval("title");
    }

    public void setTitle(String title) {
        getStateHelper().put("title", title);
    }

    public boolean isCloseable() {
        return (Boolean) getStateHelper().eval("closeable", true);
    }

    public void setCloseable(boolean closeable) {
        getStateHelper().put("closeable", closeable);
    }

    public boolean isHideOnMask() {
        return (Boolean) getStateHelper().eval("hideOnMask", false);
    }

    public void setHideOnMask(boolean hideOnMask) {
        getStateHelper().put("hideOnMask", hideOnMask);
    }

    public String getWidth() {
        return (String) getStateHelper().eval("width", "600px");
    }

    public void setWidth(String width) {
        getStateHelper().put("width", width);
    }

    public String getOnshow() {
        return (String) getStateHelper().eval("onshow", "");
    }

    public void setOnshow(String onshow) {
        getStateHelper().put("onshow", onshow);
    }

    public String getOnhide() {
        return (String) getStateHelper().eval("onhide", "");
    }

    public void setOnhide(String onhide) {
        getStateHelper().put("onhide", onhide);
    }

    public String getHeight() {
        return (String) getStateHelper().eval("height");
    }

    public void setHeight(String height) {
        getStateHelper().put("height", height);
    }

    public String getBodyStyle() {
        return (String) getStateHelper().eval("bodyStyle");
    }

    public void setBodyStyle(String bodyStyle) {
        getStateHelper().put("bodyStyle", bodyStyle);
    }

    public String getBodyStyleClass() {
        return (String) getStateHelper().eval("bodyStyleClass", "");
    }

    public void setBodyStyleClass(String bodyStyleClass) {
        getStateHelper().put("bodyStyleClass", bodyStyleClass);
    }

    public String getHeaderStyle() {
        return (String) getStateHelper().eval("headerStyle");
    }

    public void setHeaderStyle(String headerStyle) {
        getStateHelper().put("headerStyle", headerStyle);
    }

    public String getHeaderStyleClass() {
        return (String) getStateHelper().eval("headerStyleClass", "");
    }

    public void setHeaderStyleClass(String headerStyleClass) {
        getStateHelper().put("headerStyleClass", headerStyleClass);
    }

    @Override
    public void decode(FacesContext context) {
        decodeBehaviors(context);
    }

    protected void decodeBehaviors(FacesContext context) {
        Map<String, String> pars = context.getExternalContext().getRequestParameterMap();
        Map<String, List<ClientBehavior>> mapBeh = getClientBehaviors();
        String event = pars.get("javax.faces.behavior.event");
        List<ClientBehavior> beh = mapBeh.get(event);
        if (beh != null && !beh.isEmpty()) {
            String source = pars.get("javax.faces.source");
            String clientId = getClientId(context);
            if (clientId.equals(source)) {
                for (ClientBehavior cb : beh) {
                    cb.decode(context, this);
                }
            }
        }
    }
}
