package ru.java.fun.bootstrap.datatable.sorting;

import java.util.List;
import java.util.Map;

/**
 * @author Terentjev Dmitry
 *         Date: 03.02.14
 *         Time: 13:08
 */
public interface Sortable<T>{

    void nextSort(String field);

    List<T> sort(String field, List<T> data);

    public Map<String,String> getOrders();
    public Map<String,String> getAltOrders();

}
