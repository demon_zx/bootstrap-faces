package ru.java.fun.bootstrap.html;

import com.google.gson.Gson;
import ru.java.fun.bootstrap.ResponseWriterExtended;
import ru.java.fun.bootstrap.ui.UIIcon;
import ru.java.fun.bootstrap.ui.UIJson;

import javax.faces.component.UIComponent;
import javax.faces.component.UIOutput;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import javax.faces.render.FacesRenderer;
import javax.faces.render.Renderer;
import java.io.IOException;

/**
 * @author Terentjev Dmitry
 *         Date: 09.01.14
 *         Time: 9:55
 */
@FacesRenderer(rendererType = "ru.java.fun.bootstrap.Json", componentFamily = UIOutput.COMPONENT_FAMILY)
public class HtmlJson extends Renderer {

    @Override
    public void encodeBegin(FacesContext context, UIComponent _component) throws IOException {
        Gson gson = new Gson();
        ResponseWriterExtended w = new ResponseWriterExtended(_component,context);
        UIJson c = (UIJson) _component;
        w.startElement("span");
        w.writeAttribute("id", c.getClientId());
        w.writeAttribute("style","display:none");
        w.startElement("script");
        w.writeAttribute("type", "text/javascript");
        w.writeText("var "+c.getVarName()+"="+gson.toJson(c.getValue()));
        w.endElement("script");
        w.endElement("span");
    }

    @Override
    public boolean getRendersChildren() {
        return false;
    }

}
