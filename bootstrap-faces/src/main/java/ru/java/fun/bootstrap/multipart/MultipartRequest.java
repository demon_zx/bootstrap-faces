package ru.java.fun.bootstrap.multipart;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.util.Enumeration;
import java.util.Map;

/**
 * @author Terentjev Dmitry
 *         Date: 07.04.2014
 *         Time: 16:34
 */
public class MultipartRequest extends HttpServletRequestWrapper {

    public MultipartRequest(HttpServletRequest httpServletRequest) {
        super(httpServletRequest);
    }

    public void addParameter(String name, String value) {
        map.add(name, value);
    }

    private MultipartMap map = new MultipartMap();


    @Override
    public String getParameter(String s) {
        String[] v = map.get(s);
        if (v == null) {
            return super.getParameter(s);
        }
        return v[0];
    }

    @Override
    public Map<String, String[]> getParameterMap() {
        return super.getParameterMap();
    }

    @Override
    public Enumeration<String> getParameterNames() {
        return super.getParameterNames();
    }

    @Override
    public String[] getParameterValues(String s) {
        return super.getParameterValues(s);
    }
}
