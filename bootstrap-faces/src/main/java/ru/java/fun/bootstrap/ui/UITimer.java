package ru.java.fun.bootstrap.ui;

import javax.faces.application.ResourceDependencies;
import javax.faces.application.ResourceDependency;
import javax.faces.component.FacesComponent;
import javax.faces.component.behavior.ClientBehaviorHolder;

/**
 * @author Terentjev Dmitry
 *         Date: 07.02.14
 *         Time: 14:17
 */
@ResourceDependencies({
        @ResourceDependency(library = "bootstrap/js", name = "jquery.min.js", target = "head"),
        @ResourceDependency(library = "javax.faces", name = "jsf.js"),
        @ResourceDependency(library = "bootstrap/js", name = "bootstrap.min.js", target = "head"),
        @ResourceDependency(library = "bootstrap/js", name = "jsf-bs.js", target = "head")
})
@FacesComponent("timer")
public class UITimer extends UISimpleCommand implements ClientBehaviorHolder {
    @Override
    public String getRendererType() {
        return "ru.java.fun.bootstrap.Timer";
    }

    public Integer getInterval() {
        return (Integer) getStateHelper().eval("interval");
    }

    public void setInterval(Integer interval) {
        getStateHelper().put("interval", interval);
    }

    public String getJsEnable() {
        return (String) getStateHelper().eval("jsEnable", "true");
    }

    public void setJsEnable(String jsEnable) {
        getStateHelper().put("jsEnable", jsEnable);
    }

    public boolean isDisabled() {
        return (Boolean) getStateHelper().eval("disabled", false);
    }

    public void setDisabled(boolean disabled) {
        getStateHelper().put("disabled", disabled);
    }
}
