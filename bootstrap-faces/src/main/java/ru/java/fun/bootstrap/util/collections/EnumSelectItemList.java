package ru.java.fun.bootstrap.util.collections;

import javax.faces.model.SelectItem;
import java.util.ArrayList;

/**
 * @author Terentjev Dmitry
 *         Date: 06.05.2014
 *         Time: 11:34
 */
public class EnumSelectItemList extends ArrayList<SelectItem> {

    public EnumSelectItemList(Enum[] enumValues) {
        for (Enum t : enumValues) {
            add(new SelectItem(t.ordinal(), t.toString()));
        }
    }
}
