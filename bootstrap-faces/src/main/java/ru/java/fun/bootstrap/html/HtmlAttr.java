package ru.java.fun.bootstrap.html;

import ru.java.fun.bootstrap.ui.UIAttr;

import javax.faces.component.UIComponent;
import javax.faces.component.UIOutput;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import javax.faces.render.FacesRenderer;
import javax.faces.render.Renderer;
import java.io.IOException;

/**
 * @author Terentjev Dmitry
 *         Date: 09.01.14
 *         Time: 9:55
 */
@FacesRenderer(rendererType = "ru.java.fun.bootstrap.Attr", componentFamily = UIOutput.COMPONENT_FAMILY)
public class HtmlAttr extends Renderer {

    @Override
    public void encodeBegin(FacesContext context, UIComponent component) throws IOException {
        ResponseWriter w = context.getResponseWriter();
        UIAttr c = (UIAttr) component;
        w.startElement("script", c);
        w.writeAttribute("type", "text/javascript", "");
        String selector = null;
        if (c.getFor() != null) {
            selector = "[id $=" + c.getFor().replaceAll(":", "\\\\\\\\:") + "]";
        }
        if (c.getSelector() != null) {
            selector = c.getSelector();
        }
        if (selector == null) {
            selector = "#" + c.getParent().getClientId().replaceAll(":", "\\\\\\\\:");
        }
        w.writeText("$(function(){$('" + selector + "').attr('" + c.getName() + "','" + c.getValue() + "')})", "");
        w.endElement("script");
    }
}
