package ru.java.fun.bootstrap.html;

import ru.java.fun.bootstrap.ResponseWriterExtended;
import ru.java.fun.bootstrap.ui.UIButtonGroup;

import javax.faces.component.UIComponent;
import javax.faces.component.UIPanel;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import javax.faces.render.FacesRenderer;
import javax.faces.render.Renderer;
import java.io.IOException;

/**
 * @author Terentjev Dmitry
 *         Date: 09.01.14
 *         Time: 9:55
 */
@FacesRenderer(rendererType = "ru.java.fun.bootstrap.ButtonGroup", componentFamily = UIPanel.COMPONENT_FAMILY)
public class HtmlButtonGroup extends Renderer {

    @Override
    public void encodeBegin(FacesContext context, UIComponent component) throws IOException {
        ResponseWriterExtended w = new ResponseWriterExtended(component, context.getResponseWriter());
        UIButtonGroup c = (UIButtonGroup) component;
        w.startElement("div");
        w.writeAttribute("class", "btn-group " + c.getStyleClass());
        w.writeAttribute("id",c.getClientId());
        if (c.getStyle() != null)
            w.writeAttribute("style", c.getStyle());
    }

    @Override
    public void encodeEnd(FacesContext context, UIComponent uiComponent) throws IOException {
        ResponseWriter w = context.getResponseWriter();
        w.endElement("div");
    }


    protected enum PropertyKeys {
        style, styleClass
    }

}
