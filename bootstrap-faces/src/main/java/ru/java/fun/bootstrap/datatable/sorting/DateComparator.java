package ru.java.fun.bootstrap.datatable.sorting;

import java.text.Collator;
import java.util.Date;

/**
 * @author Terentjev Dmitry
 *         Date: 03.02.14
 *         Time: 13:18
 */
public class DateComparator<T> extends DefaultComparator<T> {

    private Get<T, Date> get;
    private String fieldName;

    public DateComparator(String fieldName, Get<T, Date> get) {
        this.get = get;
        this.fieldName = fieldName;
    }

    @Override
    public int compare(T o1, T o2) {
        if (getState() == SortableState.unsorted)
            return 0;
        Date v1 = get.value(o1);
        Date v2 = get.value(o2);
        if (v1 == null && v2 == null)
            return 0;
        if (getState() == SortableState.ascending) {
            return v1 != null ? v1.compareTo(v2) : -1;
        } else {
            return v2 != null ? v2.compareTo(v1) : -1;
        }
    }

    @Override
    public String getFieldName() {
        return fieldName;
    }

}
