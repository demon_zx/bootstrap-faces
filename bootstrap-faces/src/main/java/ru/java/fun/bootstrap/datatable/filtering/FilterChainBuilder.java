package ru.java.fun.bootstrap.datatable.filtering;

import ru.java.fun.bootstrap.datatable.sorting.Get;

/**
 * @author Terentjev Dmitry
 *         Date: 29.05.2014
 *         Time: 17:06
 */
public class FilterChainBuilder<T> {

    private FilterChain<T> start, current;

    public FilterChainBuilder(Get<T, String> get) {
        this(get, true);
    }

    public FilterChainBuilder(Get<T, String> get, boolean ignoreCase) {
        this.start = create(get, ignoreCase);
        current = this.start;
    }

    public FilterChainBuilder<T> add(Get<T, String> get) {
        return add(get, true);
    }

    public FilterChainBuilder<T> add(Get<T, String> get, boolean ignoreCase) {
        current = current.addNextFilter(create(get, ignoreCase));
        return this;
    }

    public FilterChain<T> build() {
        return start;
    }

    private FilterChain<T> create(Get<T, String> get, boolean ignoreCase) {
        if (ignoreCase) {
            return new IgnoreCaseFilterChain<T>(get);
        } else {
            return new CaseFilterChain<T>(get);
        }
    }
}
