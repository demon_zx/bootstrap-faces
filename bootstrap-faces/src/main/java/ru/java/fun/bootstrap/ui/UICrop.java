package ru.java.fun.bootstrap.ui;

import javax.faces.application.ResourceDependencies;
import javax.faces.application.ResourceDependency;
import javax.faces.component.FacesComponent;

/**
 * @author Terentjev Dmitry
 *         Date: 11.12.13
 *         Time: 10:09
 */
@FacesComponent("crop")
@ResourceDependencies({
        @ResourceDependency(name = "bootstrap/css/bootstrap.min.css", target = "head"),
        @ResourceDependency(name = "bootstrap/css/bootstrap-theme.min.css", target = "head"),
        @ResourceDependency(name = "bootstrap/css/jsf-bs-crop.css", target = "head"),
        @ResourceDependency(library = "bootstrap/js", name = "jquery.min.js", target = "head"),
        @ResourceDependency(library = "javax.faces", name = "jsf.js"),
        @ResourceDependency(library = "bootstrap/js", name = "bootstrap.min.js", target = "head"),
        @ResourceDependency(library = "bootstrap/js", name = "jsf-bs.js", target = "head")
})
public class UICrop extends UIStyledOutput {

    @Override
    public String getRendererType() {
        return "ru.java.fun.bootstrap.Crop";
    }

    public Integer getHeight() {
        return (Integer) getStateHelper().eval("height", 20);
    }

    public void setHeight(Integer height) {
        getStateHelper().put("height", height);
    }

    public boolean isSingleLine() {
        return (Boolean) getStateHelper().eval("singleLine", true);
    }

    public void setSingleLine(boolean singleLine) {
        getStateHelper().put("singleLine", singleLine);
    }
}
