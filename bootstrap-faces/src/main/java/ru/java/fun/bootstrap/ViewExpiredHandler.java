package ru.java.fun.bootstrap;

import javax.faces.FacesException;
import javax.faces.application.ViewExpiredException;
import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerWrapper;
import javax.faces.event.ExceptionQueuedEvent;
import javax.faces.event.ExceptionQueuedEventContext;
import java.util.Iterator;

/**
 * @author Terentjev Dmitry
 *         Date: 27.12.12
 *         Time: 15:27
 */
public class ViewExpiredHandler extends ExceptionHandlerWrapper {

    public static final String VIEW_EXPIRED_ATTRIBUTE = "_bootstrap_jsf_view_expired_";
    private ExceptionHandler wrapper;

    public ViewExpiredHandler(ExceptionHandler wrapper) {
        this.wrapper = wrapper;
    }

    @Override
    public ExceptionHandler getWrapped() {
        return wrapper;
    }

    @Override
    public void handle() throws FacesException {
        Iterator<ExceptionQueuedEvent> i = getUnhandledExceptionQueuedEvents().iterator();
        while (i.hasNext()) {
            ExceptionQueuedEvent event = i.next();
            ExceptionQueuedEventContext context =
                    (ExceptionQueuedEventContext) event.getSource();
            if (!context.getContext().getPartialViewContext().isAjaxRequest()) {
                Throwable t = context.getException();
                Throwable _t = getRootCause(t);
                if (_t == null)
                    _t = t;
                if (t instanceof ViewExpiredException) {
                    context.getContext().getExternalContext().getSessionMap().put(VIEW_EXPIRED_ATTRIBUTE, _t.getMessage());
                    i.remove();
                }
            }
        }
        //let the parent handle the rest
        getWrapped().handle();
    }

    private Throwable getFirstEx(Throwable t) {
        if (t.getCause() == null)
            return t;
        return getFirstEx(t.getCause());
    }

}
