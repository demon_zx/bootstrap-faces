package ru.java.fun.bootstrap.datatable.sorting;

/**
 * @author Terentjev Dmitry
 *         Date: 03.02.14
 *         Time: 13:11
 */
public enum SortableState {

    unsorted, ascending, descending
}
