package ru.java.fun.bootstrap.behavior;

import javax.faces.component.UIComponent;
import javax.faces.component.UIOutput;
import javax.faces.component.behavior.*;
import javax.faces.render.ClientBehaviorRenderer;
import javax.faces.render.FacesBehaviorRenderer;

/**
 * @author Terentjev Dmitry
 *         Date: 03.04.2014
 *         Time: 11:35
 */
public class TestBehavior extends AjaxBehavior {

    @Override
    public String getScript(ClientBehaviorContext behaviorContext) {
//        return "return confirm('" + message + "')";

        return "alert('ab');"+super.getScript(behaviorContext);
    }





    private String getVal(UIComponent c) {
        if (c instanceof UIOutput) {
            Object v = ((UIOutput) c).getValue();
            return v == null ? null : v.toString();
        }
        return null;
    }

    private String message, style = "font-size:14pt", styleClass = "", labelYes = "Yes", labelNo = "No", title = "Confirmation";

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getLabelYes() {
        return labelYes;
    }

    public void setLabelYes(String labelYes) {
        this.labelYes = labelYes;
    }

    public String getLabelNo() {
        return labelNo;
    }

    public void setLabelNo(String labelNo) {
        this.labelNo = labelNo;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getStyleClass() {
        return styleClass;
    }

    public void setStyleClass(String styleClass) {
        this.styleClass = styleClass;
    }



}
