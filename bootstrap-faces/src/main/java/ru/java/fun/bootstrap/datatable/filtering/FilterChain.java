package ru.java.fun.bootstrap.datatable.filtering;

import ru.java.fun.bootstrap.datatable.sorting.Get;

/**
 * @author Terentjev Dmitry
 *         Date: 30.05.2014
 *         Time: 10:30
 */
public interface FilterChain<T> {
    FilterChain<T> addNextFilter(FilterChain<T> next);

    boolean isAccepted(T entity, String filter);
}
