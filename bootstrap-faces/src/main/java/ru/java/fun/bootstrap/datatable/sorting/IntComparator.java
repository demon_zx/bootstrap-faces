package ru.java.fun.bootstrap.datatable.sorting;

import java.text.Collator;

/**
 * @author Terentjev Dmitry
 *         Date: 03.02.14
 *         Time: 13:18
 */
public class IntComparator<T> extends DefaultComparator<T> {

    private Get<T, Integer> sorter;
    private Collator c = Collator.getInstance();
    private String fieldName;

    public IntComparator(String fieldName, Get<T, Integer> get) {
        this.sorter = get;
        this.fieldName = fieldName;
    }

    @Override
    public int compare(T o1, T o2) {
        if (getState() == SortableState.unsorted)
            return 0;
        Integer v1 = sorter.value(o1);
        Integer v2 = sorter.value(o2);
        if (getState() == SortableState.ascending) {
            return v1 - v2;
        } else {
            return v2 - v1;
        }
    }

    @Override
    public String getFieldName() {
        return fieldName;
    }

}
