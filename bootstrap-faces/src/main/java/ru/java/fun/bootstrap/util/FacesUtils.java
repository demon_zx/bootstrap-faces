package ru.java.fun.bootstrap.util;

import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.SystemEvent;
import javax.faces.event.SystemEventListener;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Created by Dmitry on 11.12.12.
 * хелпер для работы с jsf
 */
public class FacesUtils {
    static public Application getApplication() {
        return getFaces().getApplication();
    }

    static public void publishEvent(Class<? extends SystemEvent> clazz, Object source) {
        getApplication().publishEvent(getFaces(), clazz, source);
    }

    static public void subscribeToEvent(Class<? extends SystemEvent> clazz, SystemEventListener listener) {
        getApplication().subscribeToEvent(clazz, listener);
    }

    static public HttpSession getSession() {
        return getRequest().getSession();
    }

    static public HttpServletRequest getRequest() {
        return (HttpServletRequest) getContext().getRequest();
    }

    static public void addMessage(String mes) {
        addMessage(mes, false);
    }

    static public void addMessageFormattedFor(String forId, String mes, Object... args) {
        addMessageFormattedFor(forId, mes, false, args);
    }

    static public void addMessageFormattedFor(String forId, String mes, boolean error, Object... args) {
        addMessageFor(forId, String.format(mes, args), error);
    }

    static public void addErrorFormattedFor(String forId, String mes, Object... args) {
        addMessageFor(forId, String.format(mes, args), true);
    }

    static public void addMessageFormatted(String mes, Object... args) {
        addMessageFormatted(mes, false, args);
    }

    static public void addMessageFormatted(String mes, boolean error, Object... args) {
        addMessage(String.format(mes, args), error);
    }

    static public void addErrorFormatted(String mes, Object... args) {
        addMessage(String.format(mes, args), true);
    }

    static public void addMessage(String mes, boolean error) {
        getFaces().addMessage(null, createMessage(mes, error));
    }

    static public void addError(String mes) {
        getFaces().addMessage(null, createMessage(mes, true));
    }

    static public void addMessage(Exception e) {
        getFaces().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
    }

    static public void addError(Exception e) {
        getFaces().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
    }

    static public FacesMessage createMessage(String mes) {
        return createMessage(mes, false);
    }

    static public FacesMessage createMessage(String mes, boolean error) {
        return new FacesMessage(severity(error), mes, mes);
    }

    static public void addMessageFor(String forId, String mes) {
        addMessageFor(forId, mes, false);
    }

    static public void addMessageFor(String forId, String mes, boolean error) {
        getFaces().addMessage(forId, new FacesMessage(severity(error), mes, ""));
    }

    static public void addErrorFor(String forId, String mes) {
        getFaces().addMessage(forId, new FacesMessage(severity(true), mes, ""));
    }

    static private FacesMessage.Severity severity(boolean error) {
        return error ? FacesMessage.SEVERITY_ERROR : FacesMessage.SEVERITY_INFO;
    }

    static public FacesContext getFaces() {
        return FacesContext.getCurrentInstance();
    }

    static public ExternalContext getContext() {
        return getFaces().getExternalContext();
    }

    static public Object getSessionAttr(String key) {
        return getContext().getSessionMap().get(key);
    }

    static public void putSessionAttr(String key, Object val) {
        getContext().getSessionMap().put(key, val);
    }

    static public String getFileBase(String fl) {
        return ((ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext()).getRealPath(fl);
    }

    static public Object getRow(String rowName) {
        return getRequestAttr(rowName);
    }

    static public Object getRequestAttr(String key) {
        return getFaces().getExternalContext().getRequestMap().get(key);
    }

    static public String getBundleValue(String bundle, String key) {
        return getBundleValue(bundle,key,"");
    }
    static public String getBundleValue(String bundle, String key, String _defaultIfEmpty) {
        Locale l = FacesContext.getCurrentInstance().getViewRoot().getLocale();
        ResourceBundle bndl = ResourceBundle.getBundle(bundle, l);
        String v = bndl.getString(key);
        return v.isEmpty() ? _defaultIfEmpty : v;
    }

    static public Long getBundleValue(String bundle, String key, long _default) {
        String v = getBundleValue(bundle, key, String.valueOf(_default));
        return Long.parseLong(v);
    }

    static public Integer getBundleValue(String bundle, String key, int _default) {
        String v = getBundleValue(bundle, key, String.valueOf(_default));
        return Integer.parseInt(v);
    }

    static public Double getBundleValue(String bundle, String key, double _default) {
        String v = getBundleValue(bundle, key, String.valueOf(_default));
        return Double.parseDouble(v);
    }
}
