$(document).ready(function () {
    jsf.bootstrap = function ($) {
        var modals = [];
        var _ok = function (data) {
            if (data) {
                if (data.responseXML) {
                    if ($(data.responseXML).find('error').size() == 0) {
                        return data.status == 'success';
                    }
                } else {
                    return data.status == 'success';
                }
                return false;
            }
            return true;
        };

        var _find = function (_this) {
            if ($(_this).hasClass('modal')) {
                return $(_this).attr('id');
            } else {
                return _find($(_this).parent());
            }
        };

        var _isShow = function (s) {
            return $(s).css('display') != 'none';
        };
        var _stopPropagation = function (event) {
            if (event && event.stopPropagation) {
                event.stopPropagation();
            }
        };
        var _modal = function (selector) {
            var sel;
            if (typeof selector == 'object') {
                sel = _find(selector);
            } else {
                var mid = $('[modal-id=' + selector.replace(/:/g, '\\:') + ']');
                if (mid.size() == 1) {
                    sel = mid.attr('id');
                } else {
                    sel = selector;
                }
            }
            sel = '#' + sel.replace(/:/g, '\\:');
            var win = modals[sel];
            if ($(sel).size() != 0) {
                if (!win) {
                    win = function () {
                        var s = sel;
                        var _onShowCallback = "";
                        var _onHideCallback = "";
                        var md = {
                            onShow: function (callback) {
                                _onShowCallback = callback;
                                return md;
                            },
                            onHide: function (callback) {
                                _onHideCallback = callback;
                                $(sel).find('.modal-content').click(function (event) {
                                    _stopPropagation(event);
                                });
                                $(sel).click(callback);
//                            $(sel).find('[data-dismiss=modal]').click(callback);
                                return md;
                            },
                            show: function (data) {
                                if (_ok(data)) {
                                    if (!_isShow(s)) {
                                        $(s).modal('show');
                                        if (_onShowCallback && _onShowCallback != '') {
                                            _onShowCallback();
                                        }
                                    }
                                }
                            },
                            hide: function (data) {
                                if (_ok(data)) {
                                    if (_onHideCallback && _onHideCallback != '') {
                                        _onHideCallback();
                                    }
                                    $(s).modal('hide');
                                }
                            },
                            hideOk: function (data) {
                                if (_ok(data)) {
                                    var $s = $(s);
                                    var $err = $('#' + $s.data('id-severity'));
                                    var err = !$err || $err.text() == '' || $err.text().indexOf('INFO') == 0;
                                    if (err) {
                                        if (_onHideCallback && _onHideCallback != '') {
                                            _onHideCallback();
                                        }
                                        $(sel).modal('hide');
                                    }
                                }
                            },
                            isShow: function () {
                                return _isShow(s);
                            },
                            selector: sel
                        };
                        return md;
                    }();
                    modals[sel] = win;
                }
            } else {
                alert('Not found modal ' + selector);
            }
            return win;
        };
        var _menu = function (id) {
            var $menu = $(id);
            var $next = $('body');
            var pad = $menu.data('top-padding');
            if ($menu.width() < 769) {
                $menu.removeClass('navbar-fixed-top');
                $next.css('padding-top', '0')
            } else {
                $menu.addClass('navbar-fixed-top');
                $next.css('padding-top', ($menu.height() + pad) + 'px');
            }
        };
        var _crop = function (id) {
            var $c = $(id);
            var h = $c.css('height');
            $c.click(function () {
                var t = $(this);
                if (t.hasClass('-jbs-crop')) {
                    t.removeClass('-jbs-crop');
                    t.removeClass('-jbs-crop-wrap');
                    t.css('min-height', t.css('height'));
                    t.css('height', '');
                } else {
                    t.addClass('-jbs-crop');
                    t.css('height', t.css('min-height'));
                    t.css('min-height', '');
                    if (t.data('single')) {
                        t.addClass('-jbs-crop-wrap');
                    }
                }
            });
        };
        var _auto = function (id, data, options) {
            var source = new Bloodhound({
                datumTokenizer: function (d) {
                    return Bloodhound.tokenizers.whitespace(d.k);
                },
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                local: data,
                limit: options.limit,
                minLenght: options.min
            });
            source.initialize();
            var $t = $(id);
            $t.attr('autocomplete', 'off');
            $t.addClass('typeahead');
            $t.typeahead(null, {
                displayKey: 'k',
                source: source.ttAdapter()
            });
            if ($t.hasClass('form-control')) {
                $t.parent().css('width', '100%');
            }
        };
        var _submit = function (formId, id, target) {
            var form = document.getElementById(formId);
            var fake = document.createElement("input");
            var save = form.target;
            fake.type = "hidden";
            fake.name = id;
            fake.value = id;
            form.appendChild(fake);
            if (target) {
                form.target = target;
            }
            form.submit();
            form.removeChild(fake);
            form.target = save;

        };
        var _create_modal = function (id, closeable) {
            $('body').append('<div id="' + id + '" class="modal fade" ' + (closeable ? 'aria-hidden="true" data-backdrop="true"' : 'aria-hidden="false" data-backdrop="static"') + '>' +
            '<div class="modal-dialog"><div class="modal-content">' +
            '<div class="modal-header">' +
            (closeable ? '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' : '') +
            '<h4 class="modal-title"></h4>' +
            '</div><div class="modal-body">' +
            '</div><div class="modal-footer">' +
            '</div></div></div></div>');
        };
        var _expire = function (id, title, message) {
            var _id = '#' + id;
            if ($(_id).length == 0) {
                _create_modal(id, false);
                var $m = $(_id);
                $m.find('.modal-title').html(title);
                $m.find('.modal-body').html('<h3>' + message + '</h3>');
                var $f = $m.find('.modal-footer');
                $f.append('<button type="button" class="btn btn-warning" data-dismiss="modal"><span class="glyphicon glyphicon-refresh"/> </button>');
                $f.find('.btn').click(function () {
                    window.location.reload();
                });
            }
            jsf.ajax.addOnError(function (data) {
                    if (data.responseXML)
                        if ($(data.responseXML).find('error-name').text() == 'class javax.faces.application.ViewExpiredException') {
                            _modal(id).show();
                        }
                }
            )
        };


        var _confirm = function (src, id, title, message, yes, no, style, styleClass) {
            var $c = $('#' + id);
            if ($c.length == 0) {
                _create_modal(id, true);
                $c = $('#' + id);
                $c.find('.modal-footer').append('<button type="button" class="btn btn-warning" data-dismiss="modal"></button>' +
                '<button type="button" class="btn btn-default"></button>');
                $c.find('.modal-footer .btn-warning').click(function () {
                    $c.data('result', '1');
                    _modal(id).hide();
                    $(src).click();
                });
                $c.find('.modal-footer .btn-default').click(function () {
                    _modal(id).hide();
                });
            }
            if ($c.data('result') == '1') {
                $c.data('result', '0');
                return true;
            } else {
                if (!_modal(id).isShow()) {
                    $c.find('.modal-title').html(title);
                    $c.find('.modal-body').html('<p style="' + style + '" class="' + styleClass + '">' + message + "</p>");
                    $c.find('.modal-footer .btn-warning').html(yes);
                    $c.find('.modal-footer .btn-default').html(no);
                    $c.data('result', '0');
                    _modal(id).show();
                    return false;
                }
            }

        };
        return {
            modal: function (selector) {
                return _modal(selector);
            },
            menuInit: function (id) {
                _menu(id);
            },
            crop: function (id) {
                _crop(id);
            },
            autocomplete: function (id, data, limit, min) {
                _auto(id, data, {limit: limit, min: min});
            },
            submit: function (form, cmp, target) {
                _submit(form, cmp, target)
            },
            confirm: function (src, id, title, message, yes, no, style, styleClass) {
                return _confirm(src, id, title, message, yes, no, style, styleClass);
            },
            expire: function (id, title, message) {
                _expire(id, title, message);
            }
        }
    }(jQuery);
});
