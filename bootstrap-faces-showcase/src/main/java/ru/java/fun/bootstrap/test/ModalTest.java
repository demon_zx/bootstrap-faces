package ru.java.fun.bootstrap.test;

import ru.java.fun.bootstrap.util.FacesUtils;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.SelectItem;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * @author Terentjev Dmitry
 *         Date: 05.02.14
 *         Time: 15:33
 */
@ManagedBean
@SessionScoped
public class ModalTest {

    public void action(){
        System.out.println("test");
    }

    public void actionError(){
        FacesUtils.addError("error");
    }

    public void actionOk(){
        FacesUtils.addMessage("ok");
    }

    public long getTime(){
        return System.currentTimeMillis();
    }
}
