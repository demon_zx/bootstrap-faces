package ru.java.fun.bootstrap.test;

import javax.faces.bean.ManagedBean;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Terentjev Dmitry
 *         Date: 12.08.2014
 *         Time: 11:45
 */
@ManagedBean(name = "confirm")
public class Confirm {
    List<Long> list;


    public Confirm() {
        list = new ArrayList<Long>();
        list.add(1L);
        list.add(2L);
    }

    public List<Long> getList() {
        return list;
    }

    public void action(Long t) {
        System.out.println("value " + t);
    }
}
