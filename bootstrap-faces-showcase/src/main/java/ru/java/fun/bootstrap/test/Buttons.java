package ru.java.fun.bootstrap.test;

import ru.java.fun.bootstrap.util.FacesUtils;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.*;

/**
 * @author Terentjev Dmitry
 *         Date: 05.02.14
 *         Time: 15:33
 */
@ManagedBean
@SessionScoped
public class Buttons implements Serializable{

    private static final long serialVersionUID = -7108293042471156812L;

    public void actionMessage(){
        FacesUtils.addMessage("Test message "+new Random().nextInt());
        FacesUtils.addMessage("Test error "+new Random().nextInt(),true);
    }

    public void actionMessage2(){
        FacesUtils.addMessageFor("messages2","test");
    }

    public void actionLog1(){
        System.out.println("Log 1");
    }

    public void actionLog2(){
        System.out.println("Log 2");
    }

    private String message = "<h3>Да ну нафиг</h3>";

    public String getMessage() {
        return message;
    }

    public List<String> confirmMessages = Arrays.asList("Confirm 1","Confirm 2");

    public List<String> getConfirmMessages() {
        return confirmMessages;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
