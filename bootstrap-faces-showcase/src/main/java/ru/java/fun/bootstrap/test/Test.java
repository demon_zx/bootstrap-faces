package ru.java.fun.bootstrap.test;

import ru.java.fun.bootstrap.util.FacesUtils;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.SelectItem;
import java.io.Serializable;
import java.util.*;

/**
 * @author Terentjev Dmitry
 *         Date: 05.02.14
 *         Time: 15:33
 */
@ManagedBean
@SessionScoped
public class Test implements Serializable{

    private static final long serialVersionUID = 2676194003369189685L;
    private String username, password;
    private boolean remember;
    private boolean disable = false;

    private Date date = new Date();

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public List<SelectItem> getSelectItems(){
        return Arrays.asList(new SelectItem("9999"), new SelectItem("Test \"html escapes\""));
    }


    public List<String> getStrings(){
        return Arrays.asList("ru","en");
    }

    private String cnt = "cnt";

    public String getCnt() {
        return cnt;
    }

    public void setCnt(String cnt) {
        this.cnt = cnt;
    }

    public void actionTimer() {
        cnt = UUID.randomUUID().toString();
    }

    private String select2, select3;


    private int select = 3;

    public String getSelect2() {
        return select2;
    }

    public void setSelect2(String select2) {
        this.select2 = select2;
    }

    public String getSelect3() {
        return select3;
    }

    public void setSelect3(String select3) {
        this.select3 = select3;
    }

    public int getSelect() {
        return select;
    }

    public void setSelect(int select) {
        this.select = select;
    }

    public void actionTestSelect() {
        System.out.println(select);
        System.out.println(select2);
        System.out.println(select3);
    }

    public boolean isDisable() {
        return disable;
    }

    public void setDisable(boolean disable) {
        this.disable = disable;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isRemember() {
        return remember;
    }

    public void setRemember(boolean remember) {
        this.remember = remember;
    }

    public void actionLogin() {
        FacesUtils.addMessage("Login");
    }

    public void actionTestHideIfNoError() {
        FacesUtils.addError(new Exception("Error exception"));
    }

    private Date begin = new Date(), end = new Date();

    public Date getBegin() {
        return begin;
    }

    public void setBegin(Date begin) {
        this.begin = begin;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public void a1() {
        System.out.println("a1");
    }

    public void a2() {
        System.out.println("a2");
    }
}
