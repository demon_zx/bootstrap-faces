package ru.java.fun.bootstrap.test.json;

/**
 * @author Terentjev Dmitry
 *         Date: 15.04.2014
 *         Time: 9:56
 */
public class JsonItem {
    private Integer id;
    private String text;

    public JsonItem() {
    }

    public JsonItem(Integer id, String text) {
        this.id = id;
        this.text = text;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
