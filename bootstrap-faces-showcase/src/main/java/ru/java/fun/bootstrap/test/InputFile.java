package ru.java.fun.bootstrap.test;

import ru.java.fun.bootstrap.util.FacesUtils;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.*;
import java.util.List;

/**
 * @author Terentjev Dmitry
 *         Date: 05.02.14
 *         Time: 15:33
 */
@ManagedBean(name = "inputfile")
@SessionScoped
public class InputFile {

    byte[] file;

    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

    List<File> files;


    public List<File> getFiles() {
        return files;
    }

    public void setFiles(List<File> files) {
        this.files = files;
    }

    public void actionUpload() throws IOException {
        if (file != null) {
            System.out.println(file.length);
        }else{
            FacesUtils.addError("Файл не загружен");
        }
    }

    public void actionUploadMultiple() {
        if (files != null)
            for (File file : files) {
                System.out.println(file);
            }
    }
}
