package ru.java.fun.bootstrap.test.input;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 * @author Terentjev Dmitry
 *         Date: 23.07.2014
 *         Time: 16:43
 */
@ManagedBean
@RequestScoped
public class SelectView {

    private String input;
    private boolean search = true;

    public boolean isSearch() {
        return search;
    }

    public void setSearch(boolean search) {
        this.search = search;
    }

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }

    public void action(){
        System.out.println(input);
    }
}
