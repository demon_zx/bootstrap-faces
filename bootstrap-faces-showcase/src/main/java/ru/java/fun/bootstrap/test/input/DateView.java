package ru.java.fun.bootstrap.test.input;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import java.util.Date;

/**
 * @author Terentjev Dmitry
 *         Date: 23.07.2014
 *         Time: 12:14
 */
@ManagedBean
@RequestScoped

public class DateView {

    private Date date,date2;

    public Date getDate2() {
        return date2;
    }

    public void setDate2(Date date2) {
        this.date2 = date2;
    }

    public void action(){
        System.out.println(date);
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
