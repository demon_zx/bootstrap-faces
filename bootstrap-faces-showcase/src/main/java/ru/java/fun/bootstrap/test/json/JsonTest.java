package ru.java.fun.bootstrap.test.json;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Terentjev Dmitry
 *         Date: 15.04.2014
 *         Time: 9:57
 */
@ManagedBean(name = "json")
@RequestScoped
public class JsonTest {

    List<JsonItem> items;

    public JsonTest(){
        items = new ArrayList<JsonItem>();
        items.add(new JsonItem(1,"Item 1"));
        items.add(new JsonItem(2,"Элемент 2"));
    }

    public List<JsonItem> getItems() {
        return items;
    }

    public void setItems(List<JsonItem> items) {
        this.items = items;
    }
}
